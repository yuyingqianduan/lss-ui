# LssSelect 下拉选择组件

## 基础用法
继承 `el-select` 所有属性，传递一个 `options` 数组数据源 即可
:::demo
LssSelect/base
:::

## 使用插槽
可以使用具名插槽来自定义下拉选项的显示内容
:::demo
LssSelect/slot
:::

## 可搜索
支持前端本地模糊搜素匹配，输入关键字后，下拉选项会根据输入内容进行匹配 配置 `filterable` 属性即可
:::demo
LssSelect/filterable
:::

## 自定义 lable value
默认是 `value` 值，`label` 值，可以通过 `label-key` 和 `value-key` 属性来自定义 `value` 值，`label` 值
:::demo
LssSelect/custom
:::

## 常用配置参数（Attributes）继承 el-select Attributes

| 参数      | 说明                                                         | 类型                                                 | 默认值 |
| :-------- | :----------------------------------------------------------- | :--------------------------------------------------- | :-----|
| model-value / v-model      | 选中项绑定值                                 | string / number / boolean / object / array         | -      |
| size      | 尺寸                                                         | 'large' / 'default' / 'small' / ''                    | -      |
| multiple  | 是否多选                                                      | boolean                                              | false   |
| clearable | 是否可清空                                                    | boolean                                              | false  |
| filterable | 是否可搜索                                                    | boolean                                             | false |
| placeholder | 占位符，默认为“Select”                                        | string                                              | -     |
| disabled  | 是否禁用                                                      | boolean                                              | false  |
| labelKey （新） | 自定义显示内容的属性名                                                      | string                                              | label  |
| valueKey （新） | 自定义 value 的属性名                                                      | string                                              | value  |
| 更多...  | [参考官方文档](https://element-plus.org/zh-CN/component/select.html)     |                                       |  |
