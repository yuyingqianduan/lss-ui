export const useFormItem = () => {
  const useElFormItem = (window as any).$useFormItem;
  let formItem = undefined;
  if (useElFormItem) {
    formItem = useElFormItem()?.formItem;
  }

  return { formItem };
};
