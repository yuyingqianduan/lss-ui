declare const LssAddress: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        options: {
            type: globalThis.PropType<any[]>;
            required: true;
            default: () => never[];
        };
        checkStrictly: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        labelKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        valueKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>>, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {}, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        options: {
            type: globalThis.PropType<any[]>;
            required: true;
            default: () => never[];
        };
        checkStrictly: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        labelKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        valueKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>>, {
        options: any[];
        checkStrictly: boolean;
        labelKey: string;
        valueKey: string;
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        options: {
            type: globalThis.PropType<any[]>;
            required: true;
            default: () => never[];
        };
        checkStrictly: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        labelKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        valueKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>>, {}, {}, {}, {}, {
        options: any[];
        checkStrictly: boolean;
        labelKey: string;
        valueKey: string;
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    options: {
        type: globalThis.PropType<any[]>;
        required: true;
        default: () => never[];
    };
    checkStrictly: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    labelKey: {
        type: globalThis.PropType<string>;
        default: string;
    };
    valueKey: {
        type: globalThis.PropType<string>;
        default: string;
    };
}>>, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {}, string, {
    options: any[];
    checkStrictly: boolean;
    labelKey: string;
    valueKey: string;
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & import('vue').Plugin))) & Record<string, any>;
export default LssAddress;
