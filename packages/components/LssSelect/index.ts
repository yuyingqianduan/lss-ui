/*
 * @Descripttion: 下拉框组件（普通）
 * @version:
 * @Author: lhl
 * @Date: 2024-07-31 10:32:36
 * @LastEditors: lhl
 * @LastEditTime: 2024-07-31 10:39:58
 */
import Select from './src/LssSelect.vue';
import { withInstall } from '../../withInstall';
const LssSelect = withInstall(Select);
export default LssSelect;
