<!--
 * @Descripttion: 组件库介绍
 * @version:
 * @Author: lhl
 * @Date: 2024-07-30 20:43:46
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-21 14:50:07
-->

# 组件库介绍

纯手工搭建 Vue3 组件库，包含多个组件，组件之间可以相互组合，满足日常开发需求， [MD语法参考](https://daringfireball.net/projects/markdown/)

- 为什么选择pnpm?

- 节省磁盘空间并提升安装速度，是同类工具速度的将近2倍
- 支持monorepo，对于组件库来说这一点很重要，不仅仅只实现了组件共享，也包含了工具（类）包、hooks、自定义指令、主题等，也可以独自发包也可以统一发包
- pnpm自带monorepo，使用monorepo的目的是多个项目可以共用一套代码风格、一套代码提交规则以及CI相关流程
- 使用 pnpm创建的node_modules是非扁平的，解决了幽灵依赖的问题

## 介绍

本组件库使用 Vite5 + Vue3 + typescript 进行二次封装日常组件，组件之间可以相互组合，满足日常开发需求

## 安装

1. 安装依赖

```
pnpm install lss-ui
```

2. 使用

```ts
// main.ts
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

import LssUI from 'lss-ui'
import 'lss-ui/lib/style.css'

const app = createApp(App)
app.use(LssUI).mount('#app')
```

3. 启动项目

```
pnpm dev
```

## 组件列表

- [x] LssEditor 按钮

## 组件使用

```html
<template>
  <div>
    <LssEditor v-model="editorValue" :height="300" />
  </div>
</template>

<script lang="ts">
  import { ref } from 'vue'
  import { LssEditor } from 'lss-ui'

  const editorValue = ref('')
</script>
```

## 基础md语法

### 插入链接

[插入链接](url)

### 插入图片

![Image](http://url/a.png)

### 插入表情

打开 [表情包链接](https://github.com/ikatyang/emoji-cheat-sheet) 随意挑选插入

### 提示语

::: tip
这是一个提示
:::

::: warning
这是一个警告
:::

::: danger
这是一个危险警告
:::

::: details
这是一个 details 标签
:::

### 自动换行

在当前行最后敲击两个空格以上

# 底部水平线

Horizontal rule:

---

`code 转义代码`
