/*
 * @Descripttion: 限定输入框输入内容格式的指令
 * @version:
 * @Author: lhl
 * @Date: 2024-08-05 11:40:31
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-05 11:49:11
 */
import type { Directive, DirectiveBinding } from 'vue'

/**
 * 自定义指令：限制输入框只能输入  数字、两位小数、三位小数等
 * @param digit: 位数，保留小数的位数 默认0，限制整数，可选参数：0|1|2|3|4等  999 只能输入不为0的正整数
 */
const limit: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding, vnode: any): void {
    // 中间变量 为了兼容输入法过程中，删掉已经输入的内容
    let flag: boolean = true
    // 兼容操作，为了处理输入法过程中，会删除旧的内容
    // 监听输入内容
    const inputHandler = (e: any) => {
      let digit: number = binding.value || 0
      let str: string = e.target.value
      let initStr = ''
      let regRule: RegExp

      // 不需要小数位的，直接限制输入数字
      if (!digit) {
        if (flag) {
          str = str
            .replace(/[^\d]/g, '')
            .replace(/^0{2,}/g, '0')
            .replace(/^0{1}[^.]/g, '0')

          e.target.value = str
          vnode.ctx.emit('update:modelValue', str)
        }
        return false
      }

      // 不为0的正整数
      if (digit == 999) {
        if (flag) {
          str = str
            .replace(/[^\d]/g, '')
            .replace(/^0{2,}/g, '0')
            .replace(/^0{1}[^.]/g, '0')
          if (str.startsWith('0')) {
            str = str.replace(/^0/, '')
          }
          e.target.value = str
          vnode.ctx.emit('update:modelValue', str)
        }
        return false
      }

      // 需要保留小数位
      for (let i: number = 0; i < digit; i++) {
        initStr += `\\\d`
      }
      regRule = new RegExp(`^(\\\-)*(\\\d+)\\\.(${initStr}).*$`)
      if (flag) {
        e.target.value = str
          .replace(/[^\d.]/g, '') // 所有非数字替换为空
          .replace(/^\./g, '') // 把第一位的.替换为空
          .replace(/\.{2,}/g, '.') // 把多个.替换成一个.
          .replace(/^0{2,}/g, '0') // 开头连续两个或更多的零开通的 替换成一个0
          .replace(/^0{1}[^.]/g, '0') // 保证开头的零后面必须是.
          .replace(regRule, '$1$2.$3')
        vnode.ctx.emit('update:modelValue', e.target.value)
      }
    }

    // 输入法输入完成
    const compositionEndHandler = (e: any) => {
      flag = true
      inputHandler(e)
    }

    // 输入法开始输入
    const compositionStartHandler = () => {
      flag = false
    }

    el.addEventListener('input', inputHandler)
    el.addEventListener('compositionend', compositionEndHandler)
    el.addEventListener('compositionstart', compositionStartHandler)
  }
}

export default limit
