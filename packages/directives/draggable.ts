/*
 * @Descripttion: 自定义指令用于实现拖拽功能
 * @version:
 * @Author: lhl
 * @Date: 2024-08-05 11:51:24
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-05 14:15:54
 */
import type { Directive } from 'vue'

const draggable: Directive = {
  mounted(el: HTMLElement) {
    let isDragging = false
    let initialX = 0
    let initialY = 0
    let initialClientX = 0
    let initialClientY = 0

    const moveHandler = (e: MouseEvent) => {
      if (!isDragging) return
      const newX = initialX + e.clientX - initialClientX
      const newY = initialY + e.clientY - initialClientY

      // 限制移动范围
      const maxLeft = window.innerWidth - el.offsetWidth
      const maxTop = window.innerHeight - el.offsetHeight
      const left = Math.min(Math.max(newX, 0), maxLeft)
      const top = Math.min(Math.max(newY, 0), maxTop)

      el.style.transform = `translate(${left}px, ${top}px)`
    }

    const upHandler = () => {
      isDragging = false
      window.removeEventListener('mousemove', moveHandler)
      window.removeEventListener('mouseup', upHandler)
    }

    el.addEventListener('mousedown', (e) => {
      isDragging = true
      const transform = window.getComputedStyle(el).transform
      const matrix = new DOMMatrixReadOnly(transform)
      initialX = matrix.m41 || 0
      initialY = matrix.m42 || 0
      initialClientX = e.clientX
      initialClientY = e.clientY

      window.addEventListener('mousemove', moveHandler)
      window.addEventListener('mouseup', upHandler)
    })

    // 防止拖拽时出现滚动条
    el.style.touchAction = 'none'
  }
}

export default draggable
