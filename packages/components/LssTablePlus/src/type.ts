import type { TableColumnCtx } from 'element-plus'

type Tag =
  | 'input'
  | 'select'
  | 'cascader'
  | 'year'
  | 'years'
  | 'month'
  | 'months'
  | 'date'
  | 'dates'
  | 'datetime'
  | 'week'
  | 'datetimerange'
  | 'daterange'
  | 'monthrange'
  | 'yearrange'
interface CustomProps {
  [key: string]: any
}

export interface Columns<T = any> extends Partial<TableColumnCtx<T>> {
  slot?: string // 自定义列的内容插槽
  headerSlot?: string // 自定义表头的插槽
}

export interface SearchColumns {
  prop?: string
  label?: string
  fields?: string[]
  type?: Tag
  rules?: []
  hide?: boolean // 是否隐藏(考虑不同权限问题的 拓展字段)
  props?: CustomProps
  placeholder?: string
  dictKey?: string // 字典名称
  defaultValue?: any
  isRest?: boolean // 添加 isRest 属性
}
