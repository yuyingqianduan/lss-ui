/*
 * @Descripttion: eslint 配置
 * @version:
 * @Author: lhl
 * @Date: 2024-07-30 22:30:20
 * @LastEditors: lhl
 * @LastEditTime: 2024-07-31 15:51:50
 */
module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-essential',
    'prettier', // 由于eslint和prettier都具备代码格式化的功能，并且可能出现冲突，所以继承eslint-config-prettier提供的配置，将eslint中冲突的配置项关闭了
    'plugin:prettier/recommended',
    './.eslintrc-auto-import.json'
  ],
  overrides: [
    {
      env: {
        node: true
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script'
      }
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  },
  plugins: ['@typescript-eslint', 'vue'],
  rules: {
    // 关闭名称校验
    'vue/multi-word-component-names': 'off',
    // 禁止 v-for 指令或范围属性的未使用变量定义 https://eslint.vuejs.org/rules/no-unused-vars.html
    'vue/no-unused-vars': 'error',
    // 禁止在与 v-for 相同的元素上使用 v-if https://eslint.vuejs.org/rules/no-use-v-if-with-v-for.html
    'vue/no-use-v-if-with-v-for': [
      'error',
      {
        allowUsingIterationVar: true
      }
    ],
    // 不允许使用未声明的变量
    'no-undef': 'error',
    // 禁止未使用的变量
    'no-unused-vars': 'error',
    // 禁止使用 var
    'no-var': 'error',
    // ts 相关
    // @ts-check 注释
    '@typescript-eslint/ban-ts-comment': ['off'],
    // 禁止使用未声明的变量
    '@typescript-eslint/no-unused-vars': ['error'],
    // 解决使用 any 类型报错
    '@typescript-eslint/no-explicit-any': ['off'],
    // 禁止使用空函数
    '@typescript-eslint/no-empty-function': 'error',
    // 禁止使用 require 语句
    '@typescript-eslint/no-var-requires': 'error'
  }
}
