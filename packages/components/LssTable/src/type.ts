export interface Columns {
  label: string
  prop?: string
  slot?: string
  headerSlot?: string
  [key: string]: any
}
