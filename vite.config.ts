/*
 * @Descripttion: 组件业务名
 * @version:
 * @Author: lhl
 * @Date: 2024-07-30 23:47:51
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-14 20:32:00
 */
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import dts from 'vite-plugin-dts'
import terser from '@rollup/plugin-terser'
import viteCompression from 'vite-plugin-compression'
import vueJsx from '@vitejs/plugin-vue-jsx' // 配置vue使用jsx
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
    dts(),
    AutoImport({
      // 你可以指定要自动导入的库
      imports: [
        'vue'
        // ...其他库
      ],
      eslintrc: {
        enabled: false, // Default `false`
        filepath: './.eslintrc-auto-import.json' // Default `./.eslintrc-auto-import.json`
      },
      dts: 'typings/auto-imports.d.ts',
      resolvers: [ElementPlusResolver()]
    }),
    Components({
      // 配置需要默认导入的自定义组件文件夹，该文件夹下的所有组件都会自动 import
      dirs: ['packages/components'],
      dts: 'typings/components.d.ts',
      resolvers: [ElementPlusResolver()]
    }),
    viteCompression({
      verbose: true,
      disable: false, // 不禁用压缩
      deleteOriginFile: false, // 压缩后是否删除原文件
      threshold: 10240, // 压缩前最小文件大小
      algorithm: 'gzip', // 压缩算法
      ext: '.gz' // 文件类型
    })
  ],
  resolve: {
    // 配置别名
    alias: {
      '@': resolve(__dirname, 'packages')
    },
    // 类型： string[] 导入时想要省略的扩展名列表。
    extensions: ['.js', '.ts', '.jsx', '.tsx', '.json', '.vue', '.mjs']
  },
  build: {
    outDir: 'lib',
    // 确保每次构建时清空输出目录
    emptyOutDir: true,
    lib: {
      entry: resolve(__dirname, 'packages/index.ts'),
      name: 'lss-ui',
      fileName: (format) => `lss-ui.${format}.js`, // 输出文件名
      formats: ['es', 'umd'] // 打包的模块格式
    },
    rollupOptions: {
      plugins: [terser()],
      external: ['vue'],
      // 指定入口文件 这样不会把src打包进去
      // input: {
      //   main: resolve(__dirname, 'packages/index.ts')
      // },
      output: {
        name: 'lss-ui',
        // format: 'es', // 确保输出格式为 ESM
        // 模块的导出是命名导出（named exports），而不是默认导出（default export）
        exports: 'named',
        globals: {
          vue: 'Vue'
        }
      }
    }
  }
})
