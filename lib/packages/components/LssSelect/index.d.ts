declare const LssSelect: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        options: {
            type: globalThis.PropType<any[]>;
            default: () => never[];
        };
        labelKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        valueKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        modelValue: {
            type: globalThis.PropType<string | number>;
            required: true;
            default: string;
        };
        showAll: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        labelAllTxt: {
            type: globalThis.PropType<string>;
            default: string;
        };
        labelAllValue: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    }, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
        "update:modelValue": (...args: any[]) => void;
    }, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        options: {
            type: globalThis.PropType<any[]>;
            default: () => never[];
        };
        labelKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        valueKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        modelValue: {
            type: globalThis.PropType<string | number>;
            required: true;
            default: string;
        };
        showAll: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        labelAllTxt: {
            type: globalThis.PropType<string>;
            default: string;
        };
        labelAllValue: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    }, {
        options: any[];
        labelKey: string;
        valueKey: string;
        modelValue: string | number;
        showAll: boolean;
        labelAllTxt: string;
        labelAllValue: string;
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        options: {
            type: globalThis.PropType<any[]>;
            default: () => never[];
        };
        labelKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        valueKey: {
            type: globalThis.PropType<string>;
            default: string;
        };
        modelValue: {
            type: globalThis.PropType<string | number>;
            required: true;
            default: string;
        };
        showAll: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        labelAllTxt: {
            type: globalThis.PropType<string>;
            default: string;
        };
        labelAllValue: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    }, {}, {}, {}, {}, {
        options: any[];
        labelKey: string;
        valueKey: string;
        modelValue: string | number;
        showAll: boolean;
        labelAllTxt: string;
        labelAllValue: string;
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    options: {
        type: globalThis.PropType<any[]>;
        default: () => never[];
    };
    labelKey: {
        type: globalThis.PropType<string>;
        default: string;
    };
    valueKey: {
        type: globalThis.PropType<string>;
        default: string;
    };
    modelValue: {
        type: globalThis.PropType<string | number>;
        required: true;
        default: string;
    };
    showAll: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    labelAllTxt: {
        type: globalThis.PropType<string>;
        default: string;
    };
    labelAllValue: {
        type: globalThis.PropType<string>;
        default: string;
    };
}>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
}, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
}, string, {
    options: any[];
    labelKey: string;
    valueKey: string;
    modelValue: string | number;
    showAll: boolean;
    labelAllTxt: string;
    labelAllValue: string;
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & ((new () => {
    $slots: {
        option?(_: {
            item: any;
        }): any;
    };
}) & import('vue').Plugin)))) & Record<string, any>;
export default LssSelect;
