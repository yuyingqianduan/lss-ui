export interface IProps {
  imgList: string[]
  btnText?: string
  noDataText?: string
  viewerOptions?: Record<string, any>
}
