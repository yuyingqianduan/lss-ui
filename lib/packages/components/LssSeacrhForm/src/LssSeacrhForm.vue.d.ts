import { PropType } from 'vue';
import { SearchFormType } from './type';

declare const _default: import('vue').DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToOption<{
    searcFiledsList: any;
    searchForm: SearchFormType;
    btnPosition?: boolean;
    searchTxt?: string;
    resetTxt?: string;
    hideBtn?: boolean;
}>, {
    searcFiledsList: () => never[];
    searchForm: () => {};
    btnPosition: boolean;
    searchTxt: string;
    resetTxt: string;
    hideBtn: boolean;
}>, {
    searchFormRef: globalThis.Ref<({
        $: import('vue').ComponentInternalInstance;
        $data: {};
        $props: Partial<{
            readonly inline: boolean;
            readonly disabled: boolean;
            readonly labelWidth: import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [StringConstructor, NumberConstructor], unknown, unknown>;
            readonly labelPosition: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "top" | "left" | "right", unknown>;
            readonly inlineMessage: boolean;
            readonly showMessage: import('element-plus/es/utils/index.mjs').EpPropMergeType<BooleanConstructor, unknown, unknown>;
            readonly requireAsteriskPosition: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "left" | "right", unknown>;
            readonly labelSuffix: string;
            readonly validateOnRuleChange: import('element-plus/es/utils/index.mjs').EpPropMergeType<BooleanConstructor, unknown, unknown>;
            readonly statusIcon: boolean;
            readonly hideRequiredAsterisk: boolean;
            readonly scrollToError: boolean;
        }> & Omit<{
            readonly disabled: boolean;
            readonly inline: boolean;
            readonly labelPosition: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "left" | "right" | "top", unknown>;
            readonly requireAsteriskPosition: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "left" | "right", unknown>;
            readonly labelWidth: import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [StringConstructor, NumberConstructor], unknown, unknown>;
            readonly labelSuffix: string;
            readonly inlineMessage: boolean;
            readonly statusIcon: boolean;
            readonly showMessage: import('element-plus/es/utils/index.mjs').EpPropMergeType<BooleanConstructor, unknown, unknown>;
            readonly validateOnRuleChange: import('element-plus/es/utils/index.mjs').EpPropMergeType<BooleanConstructor, unknown, unknown>;
            readonly hideRequiredAsterisk: boolean;
            readonly scrollToError: boolean;
            readonly size?: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "" | "default" | "small" | "large", unknown> | undefined;
            readonly model?: Record<string, any> | undefined;
            readonly rules?: Partial<Record<string, import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemRule>>> | undefined;
            readonly scrollIntoViewOptions?: import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [ObjectConstructor, BooleanConstructor], unknown, unknown> | undefined;
            onValidate?: ((prop: import('element-plus').FormItemProp, isValid: boolean, message: string) => any) | undefined;
        } & import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
            readonly model: ObjectConstructor;
            readonly rules: {
                readonly type: import('vue').PropType<Partial<Record<string, import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemRule>>>>;
                readonly required: false;
                readonly validator: ((val: unknown) => boolean) | undefined;
                __epPropKey: true;
            };
            readonly labelPosition: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, "top" | "left" | "right", unknown, "right", boolean>;
            readonly requireAsteriskPosition: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, "left" | "right", unknown, "left", boolean>;
            readonly labelWidth: import('element-plus/es/utils/index.mjs').EpPropFinalized<readonly [StringConstructor, NumberConstructor], unknown, unknown, "", boolean>;
            readonly labelSuffix: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, unknown, unknown, "", boolean>;
            readonly inline: BooleanConstructor;
            readonly inlineMessage: BooleanConstructor;
            readonly statusIcon: BooleanConstructor;
            readonly showMessage: import('element-plus/es/utils/index.mjs').EpPropFinalized<BooleanConstructor, unknown, unknown, true, boolean>;
            readonly validateOnRuleChange: import('element-plus/es/utils/index.mjs').EpPropFinalized<BooleanConstructor, unknown, unknown, true, boolean>;
            readonly hideRequiredAsterisk: BooleanConstructor;
            readonly scrollToError: BooleanConstructor;
            readonly scrollIntoViewOptions: {
                readonly type: import('vue').PropType<import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [ObjectConstructor, BooleanConstructor], unknown, unknown>>;
                readonly required: false;
                readonly validator: ((val: unknown) => boolean) | undefined;
                __epPropKey: true;
            };
            readonly size: {
                readonly type: import('vue').PropType<import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "" | "small" | "default" | "large", unknown>>;
                readonly required: false;
                readonly validator: ((val: unknown) => boolean) | undefined;
                __epPropKey: true;
            };
            readonly disabled: BooleanConstructor;
        }>> & {
            onValidate?: ((prop: import('element-plus').FormItemProp, isValid: boolean, message: string) => any) | undefined;
        }, "disabled" | "inline" | "labelPosition" | "requireAsteriskPosition" | "labelWidth" | "labelSuffix" | "inlineMessage" | "statusIcon" | "showMessage" | "validateOnRuleChange" | "hideRequiredAsterisk" | "scrollToError">;
        $attrs: {
            [x: string]: unknown;
        };
        $refs: {
            [x: string]: unknown;
        };
        $slots: Readonly<{
            [name: string]: import('vue').Slot<any> | undefined;
        }>;
        $root: ComponentPublicInstance | null;
        $parent: ComponentPublicInstance | null;
        $emit: (event: "validate", prop: import('element-plus').FormItemProp, isValid: boolean, message: string) => void;
        $el: any;
        $options: import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
            readonly model: ObjectConstructor;
            readonly rules: {
                readonly type: import('vue').PropType<Partial<Record<string, import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemRule>>>>;
                readonly required: false;
                readonly validator: ((val: unknown) => boolean) | undefined;
                __epPropKey: true;
            };
            readonly labelPosition: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, "top" | "left" | "right", unknown, "right", boolean>;
            readonly requireAsteriskPosition: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, "left" | "right", unknown, "left", boolean>;
            readonly labelWidth: import('element-plus/es/utils/index.mjs').EpPropFinalized<readonly [StringConstructor, NumberConstructor], unknown, unknown, "", boolean>;
            readonly labelSuffix: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, unknown, unknown, "", boolean>;
            readonly inline: BooleanConstructor;
            readonly inlineMessage: BooleanConstructor;
            readonly statusIcon: BooleanConstructor;
            readonly showMessage: import('element-plus/es/utils/index.mjs').EpPropFinalized<BooleanConstructor, unknown, unknown, true, boolean>;
            readonly validateOnRuleChange: import('element-plus/es/utils/index.mjs').EpPropFinalized<BooleanConstructor, unknown, unknown, true, boolean>;
            readonly hideRequiredAsterisk: BooleanConstructor;
            readonly scrollToError: BooleanConstructor;
            readonly scrollIntoViewOptions: {
                readonly type: import('vue').PropType<import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [ObjectConstructor, BooleanConstructor], unknown, unknown>>;
                readonly required: false;
                readonly validator: ((val: unknown) => boolean) | undefined;
                __epPropKey: true;
            };
            readonly size: {
                readonly type: import('vue').PropType<import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "" | "small" | "default" | "large", unknown>>;
                readonly required: false;
                readonly validator: ((val: unknown) => boolean) | undefined;
                __epPropKey: true;
            };
            readonly disabled: BooleanConstructor;
        }>> & {
            onValidate?: ((prop: import('element-plus').FormItemProp, isValid: boolean, message: string) => any) | undefined;
        }, {
            validate: (callback?: import('element-plus').FormValidateCallback | undefined) => import('element-plus').FormValidationResult;
            validateField: (props?: import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemProp> | undefined, callback?: import('element-plus').FormValidateCallback | undefined) => import('element-plus').FormValidationResult;
            resetFields: (props?: import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemProp> | undefined) => void;
            clearValidate: (props?: import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemProp> | undefined) => void;
            scrollToField: (prop: import('element-plus').FormItemProp) => void;
            fields: import('element-plus').FormItemContext[];
        }, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
            validate: (prop: import('element-plus').FormItemProp, isValid: boolean, message: string) => void;
        }, string, {
            readonly inline: boolean;
            readonly disabled: boolean;
            readonly labelWidth: import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [StringConstructor, NumberConstructor], unknown, unknown>;
            readonly labelPosition: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "top" | "left" | "right", unknown>;
            readonly inlineMessage: boolean;
            readonly showMessage: import('element-plus/es/utils/index.mjs').EpPropMergeType<BooleanConstructor, unknown, unknown>;
            readonly requireAsteriskPosition: import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "left" | "right", unknown>;
            readonly labelSuffix: string;
            readonly validateOnRuleChange: import('element-plus/es/utils/index.mjs').EpPropMergeType<BooleanConstructor, unknown, unknown>;
            readonly statusIcon: boolean;
            readonly hideRequiredAsterisk: boolean;
            readonly scrollToError: boolean;
        }, {}, string, {}> & {
            beforeCreate?: (() => void) | (() => void)[];
            created?: (() => void) | (() => void)[];
            beforeMount?: (() => void) | (() => void)[];
            mounted?: (() => void) | (() => void)[];
            beforeUpdate?: (() => void) | (() => void)[];
            updated?: (() => void) | (() => void)[];
            activated?: (() => void) | (() => void)[];
            deactivated?: (() => void) | (() => void)[];
            beforeDestroy?: (() => void) | (() => void)[];
            beforeUnmount?: (() => void) | (() => void)[];
            destroyed?: (() => void) | (() => void)[];
            unmounted?: (() => void) | (() => void)[];
            renderTracked?: ((e: import('vue').DebuggerEvent) => void) | ((e: import('vue').DebuggerEvent) => void)[];
            renderTriggered?: ((e: import('vue').DebuggerEvent) => void) | ((e: import('vue').DebuggerEvent) => void)[];
            errorCaptured?: ((err: unknown, instance: ComponentPublicInstance | null, info: string) => boolean | void) | ((err: unknown, instance: ComponentPublicInstance | null, info: string) => boolean | void)[];
        };
        $forceUpdate: () => void;
        $nextTick: typeof import('vue').nextTick;
        $watch<T extends string | ((...args: any) => any)>(source: T, cb: T extends (...args: any) => infer R ? (...args: [R, R, (cleanupFn: () => void) => void]) => any : (...args: [any, any, (cleanupFn: () => void) => void]) => any, options?: import('vue').WatchOptions): import('vue').WatchStopHandle;
    } & Omit<Readonly<globalThis.ExtractPropTypes<{
        readonly model: ObjectConstructor;
        readonly rules: {
            readonly type: import('vue').PropType<Partial<Record<string, import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemRule>>>>;
            readonly required: false;
            readonly validator: ((val: unknown) => boolean) | undefined;
            __epPropKey: true;
        };
        readonly labelPosition: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, "top" | "left" | "right", unknown, "right", boolean>;
        readonly requireAsteriskPosition: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, "left" | "right", unknown, "left", boolean>;
        readonly labelWidth: import('element-plus/es/utils/index.mjs').EpPropFinalized<readonly [StringConstructor, NumberConstructor], unknown, unknown, "", boolean>;
        readonly labelSuffix: import('element-plus/es/utils/index.mjs').EpPropFinalized<StringConstructor, unknown, unknown, "", boolean>;
        readonly inline: BooleanConstructor;
        readonly inlineMessage: BooleanConstructor;
        readonly statusIcon: BooleanConstructor;
        readonly showMessage: import('element-plus/es/utils/index.mjs').EpPropFinalized<BooleanConstructor, unknown, unknown, true, boolean>;
        readonly validateOnRuleChange: import('element-plus/es/utils/index.mjs').EpPropFinalized<BooleanConstructor, unknown, unknown, true, boolean>;
        readonly hideRequiredAsterisk: BooleanConstructor;
        readonly scrollToError: BooleanConstructor;
        readonly scrollIntoViewOptions: {
            readonly type: import('vue').PropType<import('element-plus/es/utils/index.mjs').EpPropMergeType<readonly [ObjectConstructor, BooleanConstructor], unknown, unknown>>;
            readonly required: false;
            readonly validator: ((val: unknown) => boolean) | undefined;
            __epPropKey: true;
        };
        readonly size: {
            readonly type: import('vue').PropType<import('element-plus/es/utils/index.mjs').EpPropMergeType<StringConstructor, "" | "small" | "default" | "large", unknown>>;
            readonly required: false;
            readonly validator: ((val: unknown) => boolean) | undefined;
            __epPropKey: true;
        };
        readonly disabled: BooleanConstructor;
    }>> & {
        onValidate?: ((prop: import('element-plus').FormItemProp, isValid: boolean, message: string) => any) | undefined;
    }, "validate" | "validateField" | "resetFields" | "clearValidate" | "scrollToField" | "fields"> & import('vue').ShallowUnwrapRef<{
        validate: (callback?: import('element-plus').FormValidateCallback | undefined) => import('element-plus').FormValidationResult;
        validateField: (props?: import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemProp> | undefined, callback?: import('element-plus').FormValidateCallback | undefined) => import('element-plus').FormValidationResult;
        resetFields: (props?: import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemProp> | undefined) => void;
        clearValidate: (props?: import('element-plus/es/utils/typescript.mjs').Arrayable<import('element-plus').FormItemProp> | undefined) => void;
        scrollToField: (prop: import('element-plus').FormItemProp) => void;
        fields: import('element-plus').FormItemContext[];
    }> & {} & import('vue').ComponentCustomProperties & {} & {
        $slots: {
            default?(_: {}): any;
        };
    }) | undefined>;
}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    onReset: (...args: any[]) => void;
    "select-change": (...args: any[]) => void;
    onSearch: (...args: any[]) => void;
}, string, import('vue').PublicProps, Readonly<globalThis.ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToOption<{
    searcFiledsList: any;
    searchForm: SearchFormType;
    btnPosition?: boolean;
    searchTxt?: string;
    resetTxt?: string;
    hideBtn?: boolean;
}>, {
    searcFiledsList: () => never[];
    searchForm: () => {};
    btnPosition: boolean;
    searchTxt: string;
    resetTxt: string;
    hideBtn: boolean;
}>>> & {
    onOnReset?: ((...args: any[]) => any) | undefined;
    "onSelect-change"?: ((...args: any[]) => any) | undefined;
    onOnSearch?: ((...args: any[]) => any) | undefined;
}, {
    searcFiledsList: any;
    searchForm: SearchFormType;
    btnPosition: boolean;
    searchTxt: string;
    resetTxt: string;
    hideBtn: boolean;
}, {}>;
export default _default;
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToOption<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
