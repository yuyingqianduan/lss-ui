export declare enum FormOptionsType {
    INPUT = "input",// 输入框
    SELECT = "select",// 下拉框
    CASCADER = "cascader",// 级联选择器
    DATE_PICKER = "daterange"
}
export interface SearchFormType {
    [key: string]: any;
}
