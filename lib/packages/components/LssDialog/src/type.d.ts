import { Component } from 'vue';

export declare enum TextAlign {
    LEFT = "left",
    RIGHT = "right",
    CENTER = "center",
    JUSTIFY = "justify"
}
export interface DialogProps {
    title?: string;
    confirmText?: string;
    cancelText?: string;
    headerAlign?: TextAlign;
    contentAlign?: TextAlign;
    footerAlign?: TextAlign;
    width?: string;
    closeOnClickOverlay?: boolean;
    showHeader?: boolean;
    showFooter?: boolean;
    showConfirmButton?: boolean;
    showCancelButton?: boolean;
    closeIcon?: string;
    showCloseIcon?: boolean;
    mountTo?: string;
    modelValue?: boolean;
    contentComponent?: Component | null;
    content?: string;
}
