/*
 * @Descripttion: 为 Vue 组件添加 install 方法，使其可以通过 Vue 的 app.use() 方法进行全局注册
 * @version:
 * @Author: lhl
 * @Date: 2024-07-31 10:35:32
 * @LastEditors: lhl
 * @LastEditTime: 2024-07-31 14:49:25
 */
import type { App, Plugin } from 'vue';

type SFCWithInstall<T> = T & Plugin;

export const withInstall = <T, E extends Record<string, any>>(
  main: T,
  extra?: E,
): SFCWithInstall<T> & E => {
  (main as SFCWithInstall<T>).install = (app: App) => {
    const components = [main, ...Object.values(extra ?? {})];
    for (const component of components) {
      app.component(component.name, component);
    }
  };

  if (extra) {
    Object.assign(main as Record<string, any>, extra);
  }

  return main as SFCWithInstall<T> & E;
};
