# package.json 配置介绍
1. dependencies
```json
  "dependencies": {
    "@element-plus/icons-vue": "^2.3.1",
    "@wangeditor/editor": "^5.1.23",
    "@wangeditor/editor-for-vue": "^1.0.2",
    "dayjs": "^1.11.12",
    "element-plus": "^2.7.8",
    "sass": "^1.77.8",
    "v-viewer": "^3.0.13",
    "viewerjs": "^1.11.6",
    "vitepress": "^1.3.1",
    "vue": "^3.4.34"
  },
```
2. devDependencies
```json
  "devDependencies": {
    "@rollup/plugin-terser": "^0.4.4",
    "@types/node": "^22.0.0",
    "@typescript-eslint/eslint-plugin": "^7.18.0",
    "@typescript-eslint/parser": "^7.18.0",
    "@vitejs/plugin-vue": "^5.1.1",
    "@vitejs/plugin-vue-jsx": "^4.0.0",
    "eslint": "^8.57.0",
    "eslint-plugin-vue": "^9.27.0",
    "prettier": "^3.3.3",
    "typescript": "^5.5.4",
    "unplugin-auto-import": "^0.18.2",
    "unplugin-vue-components": "^0.27.3",
    "vite": "^5.3.5",
    "vite-plugin-compression": "^0.5.1",
    "vite-plugin-dts": "4.0.0-beta.2"
  },
```
3. peerDependencies
```json
 "peerDependencies": {
    "vue": "^3.4.34"
  },
```
4. engines
```json
  "engines": {
    "node": ">= 18"
  },
```
5. browserslist
```json
  "browserslist": [
    "> 1%",
    "not ie 11",
    "not op_mini all"
  ]
```
