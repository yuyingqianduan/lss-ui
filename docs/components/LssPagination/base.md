# LssPagination 分页组件

## 基础用法

继承 `el-pagination` 所有属性 便捷使用
:::demo
LssPagination/base
:::

## 常用配置参数（Attributes）继承 el-pagination Attributes

| 参数       | 说明                                                                     | 类型                               | 默认值                               |
| :--------- | :----------------------------------------------------------------------- | :--------------------------------- | :----------------------------------- |
| layout     | 组件布局，子组件名用逗号分隔                                             | string                             | prev, pager, next, jumper, ->, total |
| size       | 尺寸                                                                     | 'large' / 'default' / 'small' / '' | -                                    |
| background | 是否为分页按钮添加背景色                                                 | boolean                            | false                                |
| small      | 是否使用小型分页样式                                                     | boolean                            | false                                |
| teleported | 是否将下拉菜单teleport至 body                                            | boolean                            | true                                 |
| disabled   | 是否禁用                                                                 | boolean                            | false                                |
| 更多...    | [参考官方文档](https://element-plus.org/zh-CN/component/pagination.html) |                                    |                                      |
