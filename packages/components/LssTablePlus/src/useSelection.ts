/*
 * @Descripttion: 表格选中问题
 * @version:
 * @Author: lhl
 * @Date: 2024-08-12 17:55:31
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-12 21:28:09
 */

import { ref, Ref } from 'vue'

export function useTableSelection() {
  // 选中的行
  const selectedList: Ref<any[]> = ref([])
  const selectedListIds: Ref<number[]> = ref([])

  const handleSelectTableData = (selection: any[]) => {
    selectedList.value = selection
    selectedListIds.value = selection.map((item: any) => item.id)
  }

  const isSelected = (id: number) => {
    return selectedListIds.value.includes(id)
  }

  return {
    selectedList,
    selectedListIds,
    handleSelectTableData,
    isSelected
  }
}
