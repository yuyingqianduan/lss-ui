# LssAddress 级联组件

## 基础用法
继承 `el-cascader` 所有属性 只需要配置一个 `options` 数据源即可
:::demo
LssAddress/base
:::

## 可搜索
只需要配置一个 `filterable` 属性即可 特别的需要防抖的话加个 `debounce` 属性
:::demo
LssAddress/filterable
:::

## 只展示最后一级
只需要配置一个 `show-all-levels="false` 即可
:::demo
LssAddress/level
:::

## 自定义 lable value
默认是 `value` 值，`label` 值，可以通过 `label-key` 和 `value-key` 属性来自定义 `value` 值，`label` 值
:::demo
LssAddress/custom
:::
## 常用配置参数（Attributes）继承 el-cascader Attributes

| 参数      | 说明                                                         | 类型                                                 | 默认值 |
| :-------- | :----------------------------------------------------------- | :--------------------------------------------------- | :-----|
| model-value / v-model      | 选中项绑定值                                 | any                                                  | -      |
| size      | 尺寸                                                         | 'large' / 'default' / 'small' / ''                    | -      |
| multiple  | 是否多选                                                      | boolean                                              | false   |
| clearable | 是否可清空                                                    | boolean                                              | false  |
| filterable | 是否可搜索                                                    | boolean                                             | false |
| placeholder | 输入框占位文本                                     | string                                              | -     |
| disabled  | 是否禁用                                                      | boolean                                              | false  |
| labelKey （新） | 自定义显示内容的属性名                                                      | string                                              | label  |
| valueKey （新） | 自定义 value 的属性名                                                      | string                                              | value  |
| 更多...  | [参考官方文档](https://element-plus.org/zh-CN/component/cascader.html)     |                                       |  |
