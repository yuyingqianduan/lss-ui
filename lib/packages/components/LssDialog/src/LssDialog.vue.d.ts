import { TextAlign, DialogProps } from './type';

declare function __VLS_template(): {
    header?(_: {}): any;
    default?(_: {}): any;
};
declare const __VLS_component: import('vue').DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToOption<DialogProps>, {
    title: string;
    confirmText: string;
    cancelText: string;
    headerAlign: TextAlign;
    contentAlign: TextAlign;
    footerAlign: TextAlign;
    width: string;
    closeOnClickOverlay: boolean;
    showHeader: boolean;
    showFooter: boolean;
    showConfirmButton: boolean;
    showCancelButton: boolean;
    closeIcon: string;
    showCloseIcon: boolean;
    mountTo: string;
    modelValue: boolean;
    content: string;
    contentComponent: null;
}>, {
    openDialog: () => void;
    closeDialog: () => void;
}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
    confirm: (...args: any[]) => void;
    cancel: (...args: any[]) => void;
}, string, import('vue').PublicProps, Readonly<globalThis.ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToOption<DialogProps>, {
    title: string;
    confirmText: string;
    cancelText: string;
    headerAlign: TextAlign;
    contentAlign: TextAlign;
    footerAlign: TextAlign;
    width: string;
    closeOnClickOverlay: boolean;
    showHeader: boolean;
    showFooter: boolean;
    showConfirmButton: boolean;
    showCancelButton: boolean;
    closeIcon: string;
    showCloseIcon: boolean;
    mountTo: string;
    modelValue: boolean;
    content: string;
    contentComponent: null;
}>>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    onConfirm?: ((...args: any[]) => any) | undefined;
    onCancel?: ((...args: any[]) => any) | undefined;
}, {
    modelValue: boolean;
    title: string;
    confirmText: string;
    cancelText: string;
    headerAlign: TextAlign;
    contentAlign: TextAlign;
    footerAlign: TextAlign;
    width: string;
    closeOnClickOverlay: boolean;
    showHeader: boolean;
    showFooter: boolean;
    showConfirmButton: boolean;
    showCancelButton: boolean;
    closeIcon: string;
    showCloseIcon: boolean;
    mountTo: string;
    contentComponent: globalThis.Component | null;
    content: string;
}, {}>;
declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, ReturnType<typeof __VLS_template>>;
export default _default;
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToOption<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
