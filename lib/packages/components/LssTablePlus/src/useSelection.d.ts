import { Ref } from 'vue';

export declare function useTableSelection(): {
    selectedList: Ref<any[]>;
    selectedListIds: Ref<number[]>;
    handleSelectTableData: (selection: any[]) => void;
    isSelected: (id: number) => boolean;
};
