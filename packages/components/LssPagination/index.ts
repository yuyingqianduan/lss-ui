import Pagination from './src/LssPagination.vue'
import { withInstall } from '../../withInstall'
const LssPagination = withInstall(Pagination)
export default LssPagination
