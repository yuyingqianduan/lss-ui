# LssViewer 图片预览组件

## 基础用法

继承 `v-viewer` 所有属性，传递一个 `imgList` 数组数据源 即可实现单张或者多张图片预览
:::demo
LssViewer/base
:::

## 无数据时候展示

默认展示暂无图片，`btnText` 支持自定义按钮文字，`noDataText` 支持自定义无数据文案 如 "-,/,xxx文案,图案等"
:::demo
LssViewer/custom
:::

## 常用配置参数（Attributes）继承 v-viewer（插件） Attributes

| 配置项            | 默认值   | 描述                                                                                                     |
| ----------------- | -------- | -------------------------------------------------------------------------------------------------------- |
| inline            | true     | 支持 inline mode                                                                                         |
| button            | true     | 右上角按钮                                                                                               |
| navbar            | true     | 底部缩略图                                                                                               |
| title             | true     | 当前图片标题                                                                                             |
| toolbar           | true     | 底部工具栏                                                                                               |
| tooltip           | true     | 显示缩放百分比                                                                                           |
| movable           | true     | 是否可以移动                                                                                             |
| zoomable          | true     | 是否可以缩放                                                                                             |
| rotatable         | true     | 是否可旋转                                                                                               |
| scalable          | true     | 是否可翻转                                                                                               |
| transition        | true     | 使用 CSS3 过度                                                                                           |
| fullscreen        | true     | 播放时是否全屏                                                                                           |
| keyboard          | true     | 是否支持键盘                                                                                             |
| btnText （新）    | 预览     | 按钮文案                                                                                                 |
| noDataText （新） | 暂无图片 | 无数据展示文案                                                                                           |
| 更多...           | -        | [中文参考文档](https://mirari.cc/posts/vue3-viewer) ，[npm 文档](https://www.npmjs.com/package/v-viewer) |
