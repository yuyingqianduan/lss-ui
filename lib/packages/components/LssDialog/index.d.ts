declare const LssDialog: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        modelValue: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        title: {
            type: globalThis.PropType<string>;
            default: string;
        };
        confirmText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        cancelText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        headerAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        contentAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        footerAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        width: {
            type: globalThis.PropType<string>;
            default: string;
        };
        closeOnClickOverlay: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showHeader: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showFooter: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showConfirmButton: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showCancelButton: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        closeIcon: {
            type: globalThis.PropType<string>;
            default: string;
        };
        showCloseIcon: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        mountTo: {
            type: globalThis.PropType<string>;
            default: string;
        };
        contentComponent: {
            type: globalThis.PropType<globalThis.Component | null>;
            default: null;
        };
        content: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
        onConfirm?: ((...args: any[]) => any) | undefined;
        onCancel?: ((...args: any[]) => any) | undefined;
    }, {
        openDialog: () => void;
        closeDialog: () => void;
    }, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
        "update:modelValue": (...args: any[]) => void;
        confirm: (...args: any[]) => void;
        cancel: (...args: any[]) => void;
    }, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        modelValue: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        title: {
            type: globalThis.PropType<string>;
            default: string;
        };
        confirmText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        cancelText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        headerAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        contentAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        footerAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        width: {
            type: globalThis.PropType<string>;
            default: string;
        };
        closeOnClickOverlay: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showHeader: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showFooter: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showConfirmButton: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showCancelButton: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        closeIcon: {
            type: globalThis.PropType<string>;
            default: string;
        };
        showCloseIcon: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        mountTo: {
            type: globalThis.PropType<string>;
            default: string;
        };
        contentComponent: {
            type: globalThis.PropType<globalThis.Component | null>;
            default: null;
        };
        content: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
        onConfirm?: ((...args: any[]) => any) | undefined;
        onCancel?: ((...args: any[]) => any) | undefined;
    }, {
        modelValue: boolean;
        title: string;
        confirmText: string;
        cancelText: string;
        headerAlign: import('./src/type').TextAlign;
        contentAlign: import('./src/type').TextAlign;
        footerAlign: import('./src/type').TextAlign;
        width: string;
        closeOnClickOverlay: boolean;
        showHeader: boolean;
        showFooter: boolean;
        showConfirmButton: boolean;
        showCancelButton: boolean;
        closeIcon: string;
        showCloseIcon: boolean;
        mountTo: string;
        contentComponent: globalThis.Component | null;
        content: string;
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        modelValue: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        title: {
            type: globalThis.PropType<string>;
            default: string;
        };
        confirmText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        cancelText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        headerAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        contentAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        footerAlign: {
            type: globalThis.PropType<import('./src/type').TextAlign>;
            default: import('./src/type').TextAlign;
        };
        width: {
            type: globalThis.PropType<string>;
            default: string;
        };
        closeOnClickOverlay: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showHeader: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showFooter: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showConfirmButton: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        showCancelButton: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        closeIcon: {
            type: globalThis.PropType<string>;
            default: string;
        };
        showCloseIcon: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        mountTo: {
            type: globalThis.PropType<string>;
            default: string;
        };
        contentComponent: {
            type: globalThis.PropType<globalThis.Component | null>;
            default: null;
        };
        content: {
            type: globalThis.PropType<string>;
            default: string;
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
        onConfirm?: ((...args: any[]) => any) | undefined;
        onCancel?: ((...args: any[]) => any) | undefined;
    }, {
        openDialog: () => void;
        closeDialog: () => void;
    }, {}, {}, {}, {
        modelValue: boolean;
        title: string;
        confirmText: string;
        cancelText: string;
        headerAlign: import('./src/type').TextAlign;
        contentAlign: import('./src/type').TextAlign;
        footerAlign: import('./src/type').TextAlign;
        width: string;
        closeOnClickOverlay: boolean;
        showHeader: boolean;
        showFooter: boolean;
        showConfirmButton: boolean;
        showCancelButton: boolean;
        closeIcon: string;
        showCloseIcon: boolean;
        mountTo: string;
        contentComponent: globalThis.Component | null;
        content: string;
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    modelValue: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    title: {
        type: globalThis.PropType<string>;
        default: string;
    };
    confirmText: {
        type: globalThis.PropType<string>;
        default: string;
    };
    cancelText: {
        type: globalThis.PropType<string>;
        default: string;
    };
    headerAlign: {
        type: globalThis.PropType<import('./src/type').TextAlign>;
        default: import('./src/type').TextAlign;
    };
    contentAlign: {
        type: globalThis.PropType<import('./src/type').TextAlign>;
        default: import('./src/type').TextAlign;
    };
    footerAlign: {
        type: globalThis.PropType<import('./src/type').TextAlign>;
        default: import('./src/type').TextAlign;
    };
    width: {
        type: globalThis.PropType<string>;
        default: string;
    };
    closeOnClickOverlay: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    showHeader: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    showFooter: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    showConfirmButton: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    showCancelButton: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    closeIcon: {
        type: globalThis.PropType<string>;
        default: string;
    };
    showCloseIcon: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    mountTo: {
        type: globalThis.PropType<string>;
        default: string;
    };
    contentComponent: {
        type: globalThis.PropType<globalThis.Component | null>;
        default: null;
    };
    content: {
        type: globalThis.PropType<string>;
        default: string;
    };
}>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    onConfirm?: ((...args: any[]) => any) | undefined;
    onCancel?: ((...args: any[]) => any) | undefined;
}, {
    openDialog: () => void;
    closeDialog: () => void;
}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
    confirm: (...args: any[]) => void;
    cancel: (...args: any[]) => void;
}, string, {
    modelValue: boolean;
    title: string;
    confirmText: string;
    cancelText: string;
    headerAlign: import('./src/type').TextAlign;
    contentAlign: import('./src/type').TextAlign;
    footerAlign: import('./src/type').TextAlign;
    width: string;
    closeOnClickOverlay: boolean;
    showHeader: boolean;
    showFooter: boolean;
    showConfirmButton: boolean;
    showCancelButton: boolean;
    closeIcon: string;
    showCloseIcon: boolean;
    mountTo: string;
    contentComponent: globalThis.Component | null;
    content: string;
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & ((new () => {
    $slots: {
        header?(_: {}): any;
        default?(_: {}): any;
    };
}) & import('vue').Plugin)))) & Record<string, any>;
export default LssDialog;
