export interface IProps {
  modelValue: string | number
  options?: any[]
  showAll?: boolean
  labelAllTxt?: string
  labelAllValue?: string
  labelKey?: string
  valueKey?: string
}


