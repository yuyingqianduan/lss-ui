/*
 * @Descripttion: useTable.ts
 * @version:
 * @Author: lhl
 * @Date: 2024-08-12 17:56:27
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-14 14:11:50
 */
import { ref } from 'vue'

interface TableRow {
  [key: string]: any
}

interface UseTableProps {
  data?: TableRow[]
}

export function useTable(props: UseTableProps) {
  const loading = ref(false)
  const data = ref<TableRow[]>([])

  const fetchTableData = async () => {
    loading.value = true
    try {
      // 假设 fetchTableData 是一个异步函数，返回表格数据
      const response = await fetchTableData()
      data.value = response
    } catch (error) {
      console.error('Failed to fetch table data:', error)
    } finally {
      loading.value = false
    }
  }

  const getTableData = async () => {
    if (props.data) {
      data.value = props.data
    } else {
      await fetchTableData()
    }
  }

  return {
    loading,
    data,
    getTableData
  }
}
