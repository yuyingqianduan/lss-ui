/*
 * @Descripttion: 组件业务名
 * @version:
 * @Author: lhl
 * @Date: 2024-08-01 21:11:05
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-05 10:43:51
 */
import { defineConfig } from 'vitepress'
import { mdPlugin } from './config/plugins'
// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'Welcome，LSS-UI',
  description: 'A VitePress Site',
  lastUpdated: true,
  // 网页角标
  head: [['link', { rel: 'icon', href: '/img/vite.svg' }]],
  // 需要部署的话
  // base: "/lss-ui/",
  markdown: {
    headers: {
      level: [0, 0]
    },
    // light: #f9fafb, dark: --vp-code-block-bg
    theme: { light: 'github-light', dark: 'github-dark' },
    config: (md) => mdPlugin(md as any)
  },
  themeConfig: {
    logo: '/img/vite.svg',
    footer: {
      copyright: 'Copyright © 2024-present lhl'
    },
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: '首页', link: '/' },
      { text: '快速开始', link: '/home/getting-started' },
      { text: '组件', link: '/components/intro' },
      { text: 'gitee地址', link: 'https://gitee.com/yuyingqianduan/lss-ui' }
    ],

    sidebar: [
      {
        text: '介绍',
        items: [
          { text: '快速开始', link: '/home/getting-started' },
          { text: '组件配置表', link: '/home/config' },
          { text: '组件', link: '/components/intro' }
        ]
      },
      {
        text: '组件使用文档',
        items: [
          { text: 'LssSelect 组件', link: '/components/LssSelect/base.md' },
          { text: 'LssAddress 级联选择器', link: '/components/LssAddress/base.md' },
          { text: 'LssEditor 组件', link: '/components/LssEditor/base.md' },
          { text: 'LssPagination 组件', link: '/components/LssPagination/base.md' },
          { text: 'LssViewer 组件', link: '/components/LssViewer/base.md' },
          { text: 'LssDialog 组件', link: '/components/LssDialog/base.md' },
          { text: 'LssTable 组件', link: '/components/LssTable/base.md' }
        ]
      }
    ],

    socialLinks: [{ icon: 'github', link: 'https://gitee.com/yuyingqianduan/lss-ui' }]
  }
})
