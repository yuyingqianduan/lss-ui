/*
 * @Descripttion: useTable.ts
 * @version:
 * @Author: lhl
 * @Date: 2024-08-12 17:56:27
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-13 15:57:28
 */
import { ref, Ref, computed, unref } from 'vue'
import { Columns, SearchColumns } from './type'
import { scrollTo } from '../../../utils/scroll-to'

interface TableParams {
  page: number
  pageSize: number
  total: number
}

interface UseTableOptions {
  columns: Columns[]
  searcFiledsList?: SearchColumns[]
  requestFn?: Function
  params?: object
  hidePage?: boolean
  hideBtn?: boolean
  autoScroll?: boolean
}

interface Field {
  prop?: string
  fields?: string[]
  defaultValue?: any
}

interface SearchForm {
  [key: string]: any
}

export function useTable(options: UseTableOptions, emits: any) {
  const { params = {}, hidePage, autoScroll, requestFn, searcFiledsList } = options

  // table loading
  const loading = ref(false)

  // 表格数据
  const data: Ref<any[]> = ref([])

  // 搜索表单
  const searchForm = ref({})

  // 表格分页参数
  const tableParams = ref<TableParams>({
    page: 1,
    pageSize: 10,
    total: 0
  })

  const searchQueryParams = () => {
    console.log(searcFiledsList, 'searcFiledsList')
    console.log(searchForm.value, 'searchForm.value')
    const list = searcFiledsList || []
    const queryParams: SearchForm = { ...searchForm.value }
    list.forEach((field) => {
      console.log(field, 'vfield', field.prop, queryParams)
      let values = field.prop && queryParams[field.prop]
      if (field.fields && field.fields.length > 0) {
        field.fields.forEach((item, index) => {
          queryParams[item] && (queryParams[item] = values[index])
          Reflect.deleteProperty(queryParams, field.prop as string)
        })
      }
    })
    return queryParams
  }

  // 搜索入参
  const searchParams = computed(() => {
    const customParams = hidePage ? {} : { ...unref(tableParams) }
    return {
      ...customParams,
      ...params,
      ...searchQueryParams()
    }
  })

  const handleSearch = () => {
    tableParams.value.page = 1
    getTableData()
    console.log(searchParams.value, 'searchParams.value')

    emits('submit', searchParams.value)
  }

  const handleReset = () => {
    searcFiledsList?.forEach((field) => {
      if (field.isRest !== false) {
        Reflect.deleteProperty(searchForm.value, field.prop as string)
      }
    })
    tableParams.value.page = 1
    getTableData()
    emits('submit', searchParams.value)
  }

  const handlePageChange = (pageParams: any) => {
    if (autoScroll) {
      scrollTo(0, 800)
    }
    tableParams.value.page = pageParams.page
    tableParams.value.pageSize = pageParams.pageSize
    getTableData()
  }

  const getTableData = async () => {
    if (typeof requestFn !== 'function') throw new Error('requestFn must be a function')
    loading.value = true
    try {
      const res = await requestFn(searchParams.value)
      const { result, object } = res || {}
      const { list, records, total } = (result ?? object) || {}
      data.value = list || records || []
      tableParams.value.total = total
    } catch (error: any) {
      console.log(error, 'error')
    } finally {
      loading.value = false
    }
  }

  return {
    loading,
    data,
    searchForm,
    tableParams,
    handleSearch,
    handleReset,
    handlePageChange,
    getTableData
  }
}
