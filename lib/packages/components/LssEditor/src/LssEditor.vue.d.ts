import { IProps } from './type';

declare const _default: import('vue').DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToOption<IProps>, {
    modelValue: string;
    mode: string;
    uploadImage: undefined;
    uploadVideo: undefined;
    toolbarConfig: {};
    editorConfig: {};
}>, {
    editorRef: import('vue').ShallowRef<any>;
}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
}, string, import('vue').PublicProps, Readonly<globalThis.ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToOption<IProps>, {
    modelValue: string;
    mode: string;
    uploadImage: undefined;
    uploadVideo: undefined;
    toolbarConfig: {};
    editorConfig: {};
}>>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
}, {
    modelValue: string;
    mode: string;
    uploadImage: (file: File) => Promise<{
        url: string;
    }>;
    uploadVideo: (file: File) => Promise<{
        url: string;
    }>;
    toolbarConfig: any;
    editorConfig: any;
}, {}>;
export default _default;
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToOption<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
