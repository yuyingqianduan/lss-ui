/*
 * @Descripttion: 富文本组件（普通）
 * @version:
 * @Author: lhl
 * @Date: 2024-07-31 10:32:36
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-11 16:11:36
 */
import Editor from './src/LssEditor.vue'
import { withInstall } from '../../withInstall'
const LssEditor = withInstall(Editor)
export default LssEditor
