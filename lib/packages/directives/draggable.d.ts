import { Directive } from 'vue';

declare const draggable: Directive;
export default draggable;
