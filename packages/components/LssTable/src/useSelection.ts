/*
 * @Descripttion: 表格选中问题 InstanceType<typeof ElTable> 可以获取组件实例的枚举 包括属性方法
 * @version:
 * @Author: lhl
 * @Date: 2024-08-12 17:55:31
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-22 16:51:30
 */

import { ref, Ref } from 'vue'
import type { ElTable } from 'element-plus'

interface TableRow {
  [key: string]: any
}

interface UseTableSelectionProps {
  uniqueKey: string
  data?: TableRow[]
  initialSelectedIds?: (string | number)[]
}

export function useTableSelection(props: UseTableSelectionProps) {
  const selectedList: Ref<TableRow[]> = ref([])
  const selectedListIds: Ref<string[]> = ref([])

  /**
   * 表格选中
   * @param selection 简单的跨页多选的方法 （不用回显的只用这个即可实现）
   */
  const handleSelectTableData = (selection: TableRow[]) => {
    selectedList.value = selection
    selectedListIds.value = selection.map((item) => item[props.uniqueKey])
  }

  /**
   * 单选 反选
   * @param row 行数据
   */
  const handleSelect = (row: TableRow) => {
    // selection: TableRow[],原本接收两个参数
    const id = row[props.uniqueKey]
    const index = selectedListIds.value.indexOf(id)
    if (index > -1) {
      selectedListIds.value.splice(index, 1)
    } else {
      selectedListIds.value.push(id)
    }
  }

  /**
   * 全选 反选
   * @param selection 选中的行数据
   */
  const handleSelectAll = (selection: TableRow[]) => {
    if (selection.length === 0) {
      selectedListIds.value = []
    } else {
      selectedListIds.value = selection.map((row) => row[props.uniqueKey])
    }
  }
  const isSelected = (id: string) => selectedListIds.value.includes(id)

  /**
   * 回显数据的方法
   * @param initialSelectedIds 初始选中的 id
   * @param tableRef 表格的 ref
   */
  const setInitialSelectedIds = (initialSelectedIds: (string | number)[], tableRef: InstanceType<typeof ElTable>) => {
    if (Array.isArray(initialSelectedIds)) {
      // 这里需要手动设置表格的选中状态
      if (props.data) {
        tableRef?.clearSelection()
        props.data.forEach((row) => {
          // 初始化的数据是否在 props.data 存在 存在即勾选中
          if (initialSelectedIds.includes(row[props.uniqueKey])) {
            tableRef?.toggleRowSelection(row, true)
          }
        })
      }
    } else {
      console.error('initialSelectedIds 不是一个数组')
    }
  }

  return {
    selectedList,
    selectedListIds,
    isSelected,
    handleSelectTableData,
    handleSelect,
    handleSelectAll,
    setInitialSelectedIds
  }
}
