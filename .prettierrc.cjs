/*
 * @Descripttion: prettier 配置
 * @version:
 * @Author: lhl
 * @Date: 2024-07-30 22:47:39
 * @LastEditors: lhl
 * @LastEditTime: 2024-07-30 22:55:36
 */
module.exports = {
  printWidth: 120,
  tabWidth: 2,
  useTabs: false,
  // 使用单引号 (true:单引号;false:双引号)
  singleQuote: true,
  // 结尾添加分号
  semi: false,
  // 元素末尾是否加逗号，默认es5: ES5中的 objects, arrays 等会添加逗号，TypeScript 中的 type 后不加逗号
  trailingComma: "none",
  // 开始标签的右尖括号是否跟随在最后一行属性末尾，默认false
  bracketSameLine: false,
  // 对象字面量的括号之间打印空格 (true - Example: { foo: bar } ; false - Example: {foo:bar})
  bracketSpacing: true,
  // 在 JSX 中使用单引号替代双引号，默认false
  jsxSingleQuote: false,
  // 对象属性是否使用引号(as-needed | consistent | preserve;默认as-needed:对象的属性需要加引号才添加;)
  quoteProps: "as-needed",
  // (x)=>{},单个参数箭头函数是否显示小括号。(always:始终显示;avoid:省略括号。默认:always)
  arrowParens: "always",
  // vue 的script和style的内容是否缩进
  vueIndentScriptAndStyle: false,
  // 行结尾形式 mac和linux是\n  windows是\r\n
  endOfLine: "auto",
};
