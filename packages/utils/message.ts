/*
 * @Descripttion: 消息提示
 * @version:
 * @Author: lhl
 * @Date: 2024-08-03 17:38:53
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-03 17:40:35
 */

import { ElMessage } from 'element-plus'

// 消息 （默认）
const Message = (message: string, duration: number = 3000): void => {
  ElMessage({
    showClose: true,
    message,
    duration
  })
}

// 成功
const successMessage = (message: string, duration: number = 3000): void => {
  ElMessage({
    showClose: true,
    message,
    duration,
    type: 'success'
  })
}

// 警告
const warnMessage = (message: string, duration: number = 3000): void => {
  ElMessage({
    showClose: true,
    message,
    duration,
    type: 'warning'
  })
}

// 失败
const errorMessage = (message: string, duration: number = 3000): void => {
  ElMessage({
    showClose: true,
    message,
    duration,
    type: 'error'
  })
}

export { Message, successMessage, warnMessage, errorMessage }
