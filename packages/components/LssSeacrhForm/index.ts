/*
 * @Descripttion: 组件业务名
 * @version:
 * @Author: lhl
 * @Date: 2024-08-04 22:34:38
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-11 23:25:47
 */
import SeacrhForm from './src/LssSeacrhForm.vue'
import { withInstall } from '../../withInstall'
const LssSeacrhForm = withInstall(SeacrhForm)
export default LssSeacrhForm
