<!--
 * @Descripttion: 详细介绍
 * @version:
 * @Author: lhl
 * @Date: 2024-07-30 20:49:30
 * @LastEditors: lhl
 * @LastEditTime: 2024-07-30 23:34:49
-->

# 搭建技术以及搭建过程

本组件库使用 Vue3 + Typescript + Vite5 搭建，下面是搭建过程：

## 初始化工程目录

cd 到你想创建工程的目录，创建 yy-ui 目录，并进入该目录

```bash
# 生成 package.json 文件
pnpm init
```

## 安装必要依赖

```bash
pnpm i vue element-plus @element-plus/icons-vue dayjs

pnpm i vite typescript vue-tsc @vitejs/plugin-vue @vitejs/plugin-vue-jsx unplugin-auto-import unplugin-vue-components -D
```

## 代码规范约束

使用 [eslint](https://eslint.nodejs.cn/) + [prettier](https://www.prettier.cn/docs/index.html) 进行代码规范，两个前提项目中下载这两个依赖，vscode 编辑器也要安装两个插件。

```bash
pnpm i eslint prettier -D

# 执行自动生成 eslint 配置文件
npx eslint --init

# 执行自动生成 ts 配置文件 前提是 全局安装了ts（npm install -g typescript）
tsc --init
```

## 强制删除 node_modules 目录

```bash
Remove-item -Force -Recurse node_modules
```

## 报错 npm login

Sign up to CNPM
Public registration is not allowed
