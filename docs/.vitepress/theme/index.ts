/*
 * @Descripttion: 组件业务名
 * @version:
 * @Author: lhl
 * @Date: 2024-08-01 21:11:05
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-11 21:21:09
 */
// https://vitepress.dev/guide/custom-theme
// import { h } from 'vue'
import type { Theme } from 'vitepress'
import DefaultTheme from 'vitepress/theme'
import './style.css'

// 自定义主题
import Layout from './Layout.vue'

// 引入 ElementPlus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'
import locale from 'element-plus/es/locale/lang/zh-cn'
// 图标并进行全局注册
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// 基于element-plus二次封装基础组件 引用本地全局注册这样就不用每次 更新依赖包才生效 得到最新代码
import lssUI from '../../../packages'
// import lssUI from 'lss-ui'
import 'lss-ui/lib/style.css'

// 预览组件
import { VPDemo } from '../vitepress'
import '../../public/css/index.css'

export default {
  extends: DefaultTheme,
  Layout: Layout,
  // 默认的
  // () => {
  // return h(DefaultTheme.Layout, null, {
  //   // https://vitepress.dev/guide/extending-default-theme#layout-slots
  // })
  // }
  enhanceApp({ app, router, siteData }) {
    // 注册ElementPlus
    app.use(ElementPlus, {
      locale // 语言设置
    })
    // 注册所有图标
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
      app.component(key, component)
    }
    // 全局注册基础组件
    app.use(lssUI)
    // 注册预览组件
    app.component('Demo', VPDemo)
    // ...
  }
} satisfies Theme
