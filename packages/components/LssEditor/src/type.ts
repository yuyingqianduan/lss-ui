export interface IProps {
  modelValue: string
  mode?: string
  uploadImage?: (file: File) => Promise<{ url: string }>
  uploadVideo?: (file: File) => Promise<{ url: string }>
  toolbarConfig?: any
  editorConfig?: any
}
