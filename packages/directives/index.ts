/*
 * @Descripttion: 此文件放所有的自定义指令
 * @version:
 * @Author: lhl
 * @Date: 2024-08-05 11:36:40
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-05 11:57:22
 */

import type { Directive, App } from 'vue'

import draggable from './draggable'
import limit from './limitInput'

interface DirectiveType {
  [key: string]: Directive
}

// 指令列表
const directiveList: DirectiveType = {
  limit,
  draggable
}

// 批量挂载指令 对外暴漏方法
export const installDirective = {
  install(app: App<Element>) {
    Object.keys(directiveList).forEach((k) => {
      app.directive(k, directiveList[k])
    })
  }
}
