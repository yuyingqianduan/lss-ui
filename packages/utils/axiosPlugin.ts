import axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  CancelTokenSource,
  InternalAxiosRequestConfig,
  AxiosRequestHeaders
} from 'axios'
import { ElLoading, ElMessage } from 'element-plus'

export type RequestInterceptor = (
  config: InternalAxiosRequestConfig
) => InternalAxiosRequestConfig | Promise<InternalAxiosRequestConfig>
export type RequestErrorInterceptor = (error: any) => any
export type ResponseInterceptor = (response: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>
export type ResponseErrorInterceptor = (error: any) => any

export interface AxiosPluginConfig {
  baseURL?: string
  timeout?: number
  requestInterceptors?: RequestInterceptor[]
  requestErrorInterceptors?: RequestErrorInterceptor[]
  responseInterceptors?: ResponseInterceptor[]
  responseErrorInterceptors?: ResponseErrorInterceptor[]
  errorHandler?: (error: any) => void
  successHandler?: (response: AxiosResponse) => void
  commonHeaders?: Record<string, string>
  cacheEnabled?: boolean
  retryCount?: number
  loadingEnabled?: boolean
  loadingPlugin?: any
  loadingOptions?: any
  toastEnabled?: boolean
  toastOptions?: any
  maxConcurrentRequests?: number
}

interface RequestCache {
  [key: string]: CancelTokenSource
}

interface ResponseCache {
  [key: string]: AxiosResponse
}

// 扩展 InternalAxiosRequestConfig 类型
declare module 'axios' {
  interface InternalAxiosRequestConfig {
    commonHeaders?: Record<string, string>
    loadingInstance?: any
    toastEnabled?: boolean
    loadingOptions?: any
    loadingEnabled?: boolean
    retryCount?: number
    toastOptions?: any
  }
}

export default class AxiosPlugin {
  private static instance: AxiosPlugin | null = null
  private service: AxiosInstance
  private requestCache: RequestCache = {}
  private responseCache: ResponseCache = {}
  private pendingRequests: Set<string> = new Set()
  private maxConcurrentRequests: number
  private asyncQueue: any[] = []

  private constructor(config: AxiosPluginConfig) {
    this.service = axios.create({
      baseURL: config.baseURL || '/api',
      timeout: config.timeout || 3000
    })
    this.maxConcurrentRequests = config.maxConcurrentRequests || Infinity
    this.setupInterceptors(config)
  }

  public static getInstance(config: AxiosPluginConfig): AxiosPlugin {
    if (!AxiosPlugin.instance) {
      AxiosPlugin.instance = new AxiosPlugin(config)
    }
    return AxiosPlugin.instance
  }

  private setupInterceptors(config: AxiosPluginConfig): void {
    if (config.requestInterceptors) {
      config.requestInterceptors.forEach((interceptor) => {
        this.service.interceptors.request.use(interceptor)
      })
    }
    if (config.requestErrorInterceptors) {
      config.requestErrorInterceptors.forEach((interceptor) => {
        this.service.interceptors.request.use(undefined, interceptor)
      })
    }
    if (config.responseInterceptors) {
      config.responseInterceptors.forEach((interceptor) => {
        this.service.interceptors.response.use(interceptor)
      })
    }
    if (config.responseErrorInterceptors) {
      config.responseErrorInterceptors.forEach((interceptor) => {
        this.service.interceptors.response.use(undefined, interceptor)
      })
    }

    // 添加通用请求头
    if (config.commonHeaders) {
      this.service.interceptors.request.use((config) => {
        config.headers = { ...config.headers, ...config.commonHeaders } as AxiosRequestHeaders
        return config
      })
    }

    // 处理错误
    if (config.errorHandler) {
      this.service.interceptors.response.use(undefined, (error) => {
        if (typeof config.errorHandler === 'function') {
          config.errorHandler(error)
        } else {
          console.error('Error handler is not a function:', error)
        }
        return Promise.reject(error)
      })
    }

    // 处理成功
    if (config.successHandler) {
      this.service.interceptors.response.use((response) => {
        if (typeof config.successHandler === 'function') {
          config.successHandler(response)
        } else {
          console.warn('config.successHandler is not a function or is undefined')
        }
        return response // 确保返回 AxiosResponse 对象
      })
    }

    // 取消重复请求
    this.service.interceptors.request.use((config) => {
      const key = `${config.method}-${config.url}`
      if (this.requestCache[key]) {
        this.requestCache[key].cancel('Duplicate request')
      }
      const source = axios.CancelToken.source()
      config.cancelToken = source.token
      this.requestCache[key] = source
      this.pendingRequests.add(key)
      return config
    })

    this.service.interceptors.response.use(
      (response) => {
        const key = `${response.config.method}-${response.config.url}`
        this.pendingRequests.delete(key)
        this.processQueue()
        return response
      },
      (error) => {
        if (axios.isCancel(error)) {
          // 如果是取消请求，不访问 error.config
          if (typeof error.message === 'string') {
            this.pendingRequests.delete(error.message) // 确保 error.message 是字符串
          }
        } else {
          // 如果不是取消请求，可以安全地访问 error.config
          const key = `${error.config.method}-${error.config.url}`
          this.pendingRequests.delete(key)
        }
        this.processQueue()
        return Promise.reject(error)
      }
    )

    // 断网重连
    if (config.retryCount) {
      this.service.interceptors.response.use(undefined, async (error) => {
        if (axios.isCancel(error)) {
          return Promise.reject(error)
        }
        const config = error.config
        if (!config || !config.retryCount) {
          return Promise.reject(error)
        }
        config.retryCount -= 1
        const delayRetryRequest = new Promise<void>((resolve) => {
          setTimeout(() => {
            resolve()
          }, 1000)
        })
        await delayRetryRequest
        return this.service(config)
      })
    }

    // GET请求缓存
    if (config.cacheEnabled) {
      this.service.interceptors.request.use((config: any) => {
        if (config.method?.toLowerCase() === 'get') {
          const key = `${config.method}-${config.url}-${JSON.stringify(config.params || {})}`
          if (this.responseCache[key]) {
            return Promise.resolve({
              ...config,
              cacheData: this.responseCache[key]
            })
          }
        }
        return config
      })

      this.service.interceptors.response.use((response) => {
        if (response.config.method?.toLowerCase() === 'get') {
          const key = `${response.config.method}-${response.config.url}-${JSON.stringify(response.config.params || {})}`
          this.responseCache[key] = response
        }
        return response
      })
    }

    // 自定义Loading
    if (config.loadingEnabled) {
      this.service.interceptors.request.use((config) => {
        if (config.loadingEnabled !== false) {
          config.loadingInstance = ElLoading.service(config.loadingOptions || {})
        }
        return config
      })

      this.service.interceptors.response.use(
        (response) => {
          if (response.config.loadingInstance) {
            response.config.loadingInstance.close()
          }
          return response
        },
        (error) => {
          if (error.config.loadingInstance) {
            error.config.loadingInstance.close()
          }
          return Promise.reject(error)
        }
      )
    }

    // 全局Toast
    if (config.toastEnabled) {
      this.service.interceptors.response.use(
        (response) => {
          if (response.config.toastEnabled !== false) {
            ElMessage(response.config.toastOptions || {})
          }
          return response
        },
        (error) => {
          if (error.config.toastEnabled !== false) {
            ElMessage.error(error.config.toastOptions || {})
          }
          return Promise.reject(error)
        }
      )
    }

    // 最大并发数控制
    this.service.interceptors.request.use((config) => {
      const key = `${config.method}-${config.url}`
      return new Promise((resolve) => {
        const request = () => {
          this.pendingRequests.add(key)
          resolve(config)
        }
        this.asyncQueue.push(request)
        this.processQueue()
      })
    })

    this.service.interceptors.response.use(
      (response) => {
        const key = `${response.config.method}-${response.config.url}`
        this.pendingRequests.delete(key)
        this.processQueue()
        return response
      },
      (error) => {
        const key = `${error.config.method}-${error.config.url}`
        this.pendingRequests.delete(key)
        this.processQueue()
        return Promise.reject(error)
      }
    )
  }

  private async processQueue() {
    if (this.pendingRequests.size < this.maxConcurrentRequests && this.asyncQueue.length > 0) {
      const nextRequest = this.asyncQueue.shift()
      if (nextRequest) {
        nextRequest()
      }
    }
  }

  public get(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.service.get(url, config)
  }

  public post(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.service.post(url, data, config)
  }

  public put(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.service.put(url, data, config)
  }

  public delete(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.service.delete(url, config)
  }

  public patch(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse> {
    return this.service.patch(url, data, config)
  }
}
