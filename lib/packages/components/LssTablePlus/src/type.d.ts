import { TableColumnCtx } from 'element-plus';

type Tag = 'input' | 'select' | 'cascader' | 'year' | 'years' | 'month' | 'months' | 'date' | 'dates' | 'datetime' | 'week' | 'datetimerange' | 'daterange' | 'monthrange' | 'yearrange';
interface CustomProps {
    [key: string]: any;
}
export interface Columns<T = any> extends Partial<TableColumnCtx<T>> {
    slot?: string;
    headerSlot?: string;
}
export interface SearchColumns {
    prop?: string;
    label?: string;
    fields?: string[];
    type?: Tag;
    rules?: [];
    hide?: boolean;
    props?: CustomProps;
    placeholder?: string;
    dictKey?: string;
    defaultValue?: any;
    isRest?: boolean;
}
export {};
