declare const Message: (message: string, duration?: number) => void;
declare const successMessage: (message: string, duration?: number) => void;
declare const warnMessage: (message: string, duration?: number) => void;
declare const errorMessage: (message: string, duration?: number) => void;
export { Message, successMessage, warnMessage, errorMessage };
