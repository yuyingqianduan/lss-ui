# 快速上手

LSS-UI（lss-ui为npm包名） 是一个基于 vue3 + ts + Element-plus + Vite 再次封装的通用型组件

## 安装

```bash
# 推荐pnpm了
pnpm add lss-ui
&
pnpm install lss-ui
```

## 使用
```ts
// main.ts
import lssUi from 'lss-ui'
import 'lss-ui/lib/style.css'
```

## 定制化基础md语法

**Input**

```md
::: info
This is an info box.
:::

::: tip
This is a tip.
:::

::: warning
This is a warning.
:::

::: danger
This is a dangerous warning.
:::

::: details
This is a details block.
:::
```

**Output**

::: info
This is an info box.
:::

::: tip
This is a tip.
:::

::: warning
This is a warning.
:::

::: danger
This is a dangerous warning.
:::

::: details
This is a details block.
:::

## More

Check out the documentation for the [full list of markdown extensions](https://vitepress.dev/guide/markdown).
