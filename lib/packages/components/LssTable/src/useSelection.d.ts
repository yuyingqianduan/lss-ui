import { Ref } from 'vue';
import { ElTable } from 'element-plus';

interface TableRow {
    [key: string]: any;
}
interface UseTableSelectionProps {
    uniqueKey: string;
    data?: TableRow[];
    initialSelectedIds?: string[];
}
export declare function useTableSelection(props: UseTableSelectionProps): {
    selectedList: Ref<TableRow[]>;
    selectedListIds: Ref<string[]>;
    isSelected: (id: string) => boolean;
    handleSelectTableData: (selection: TableRow[]) => void;
    handleSelect: (row: TableRow) => void;
    handleSelectAll: (selection: TableRow[]) => void;
    setInitialSelectedIds: (initialSelectedIds: string[], tableRef: InstanceType<typeof ElTable>) => void;
};
export {};
