/*
 * @Descripttion: 三级联动组件
 * @version:
 * @Author: lhl
 * @Date: 2024-07-31 10:31:32
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-11 15:35:42
 */
import Address from './src/LssAddress.vue'
import { withInstall } from '../../withInstall'
const LssAddress = withInstall(Address)
export default LssAddress
