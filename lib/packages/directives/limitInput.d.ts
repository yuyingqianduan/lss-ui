import { Directive } from 'vue';

/**
 * 自定义指令：限制输入框只能输入  数字、两位小数、三位小数等
 * @param digit: 位数，保留小数的位数 默认0，限制整数，可选参数：0|1|2|3|4等  999 只能输入不为0的正整数
 */
declare const limit: Directive;
export default limit;
