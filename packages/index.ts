/*
 * @Descripttion: 全部组件导入注册
 * @version:
 * @Author: lhl
 * @Date: 2024-07-30 23:58:54
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-13 21:44:03
 */
import type { Component, App } from 'vue'

// 组件
import LssAddress from './components/LssAddress'
import LssSelect from './components/LssSelect'
import LssEditor from './components/LssEditor'
import LssPagination from './components/LssPagination'
import LssViewer from './components/LssViewer'
import LssDialog from './components/LssDialog'
import { useDialog } from './components/LssDialog/src' // 函数式调用
import LssSeacrhForm from './components/LssSeacrhForm'
import LssTable from './components/LssTable' // 扩展表格功能
import LssTablePlus from './components/LssTablePlus' // 集成分页搜索组件 通用性表格
// 公共方法

// 导出指令方法
import { installDirective } from './directives/index'

// 组件列表
const components: { [key: string]: Component } = {
  LssAddress,
  LssSelect,
  LssEditor,
  LssPagination,
  LssViewer,
  LssDialog,
  LssSeacrhForm,
  LssTable,
  LssTablePlus
}

// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install: any = (app: App): void => {
  // 遍历注册全局组件
  for (const key in components) {
    app.component(key, components[key])
  }

  // 判断是否是直接引入文件
  if (typeof window !== 'undefined' && (window as any).Vue) {
    install((window as any).Vue)
  }

  // 注册全局指令
  installDirective && app.use(installDirective)

  // 挂载公共方法
  app.config.globalProperties.$useDialog = useDialog
}

// 导出的对象必须具有 install，才能被 Vue.use() 方法安装
const lssUi = { install }

export default lssUi

// 导出公共方法，按需引入
export { useDialog, installDirective }

// 导出公共组件，按需引入
export { LssAddress, LssSelect, LssEditor, LssPagination, LssViewer, LssDialog, LssSeacrhForm, LssTable, LssTablePlus }
