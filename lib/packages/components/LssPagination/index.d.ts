declare const LssPagination: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        pageParams: {
            type: globalThis.PropType<import('./src/type').PageParams>;
            required: true;
            default: () => {
                page: number;
                size: number;
                total: number;
            };
        };
        layout: {
            type: globalThis.PropType<string>;
            default: string;
        };
        background: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        pageSizes: {
            type: globalThis.PropType<number[]>;
            default: () => number[];
        };
    }>> & {
        onPagination?: ((...args: any[]) => any) | undefined;
    }, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
        pagination: (...args: any[]) => void;
    }, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        pageParams: {
            type: globalThis.PropType<import('./src/type').PageParams>;
            required: true;
            default: () => {
                page: number;
                size: number;
                total: number;
            };
        };
        layout: {
            type: globalThis.PropType<string>;
            default: string;
        };
        background: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        pageSizes: {
            type: globalThis.PropType<number[]>;
            default: () => number[];
        };
    }>> & {
        onPagination?: ((...args: any[]) => any) | undefined;
    }, {
        pageParams: import('./src/type').PageParams;
        layout: string;
        background: boolean;
        pageSizes: number[];
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        pageParams: {
            type: globalThis.PropType<import('./src/type').PageParams>;
            required: true;
            default: () => {
                page: number;
                size: number;
                total: number;
            };
        };
        layout: {
            type: globalThis.PropType<string>;
            default: string;
        };
        background: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        pageSizes: {
            type: globalThis.PropType<number[]>;
            default: () => number[];
        };
    }>> & {
        onPagination?: ((...args: any[]) => any) | undefined;
    }, {}, {}, {}, {}, {
        pageParams: import('./src/type').PageParams;
        layout: string;
        background: boolean;
        pageSizes: number[];
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    pageParams: {
        type: globalThis.PropType<import('./src/type').PageParams>;
        required: true;
        default: () => {
            page: number;
            size: number;
            total: number;
        };
    };
    layout: {
        type: globalThis.PropType<string>;
        default: string;
    };
    background: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    pageSizes: {
        type: globalThis.PropType<number[]>;
        default: () => number[];
    };
}>> & {
    onPagination?: ((...args: any[]) => any) | undefined;
}, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    pagination: (...args: any[]) => void;
}, string, {
    pageParams: import('./src/type').PageParams;
    layout: string;
    background: boolean;
    pageSizes: number[];
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & import('vue').Plugin))) & Record<string, any>;
export default LssPagination;
