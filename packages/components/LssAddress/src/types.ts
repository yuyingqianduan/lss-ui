/*
 * @Descripttion: 组件业务名
 * @version:
 * @Author: lhl
 * @Date: 2024-07-31 10:31:32
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-01 11:09:50
 */
export interface IProps {
  options: any[]
  checkStrictly?: boolean
  labelKey?: string
  valueKey?: string
}
