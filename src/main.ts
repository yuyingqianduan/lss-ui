import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import lssUi from '../packages'

const app = createApp(App)
app.use(lssUi)
app.mount('#app')
