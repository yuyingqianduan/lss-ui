import { DialogProps } from './type';

export declare function useDialog(options: DialogProps): void;
