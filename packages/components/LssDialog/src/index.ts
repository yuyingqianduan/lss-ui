/*
 * @Descripttion: 弹窗 函数式调用
 * @version:
 * @Author: lhl
 * @Date: 2024-08-04 22:45:49
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-11 09:07:55
 */
import { createApp, h } from 'vue'
import LssDialog from './LssDialog.vue'
import { DialogProps } from './type'

let instance: any = null
let container: HTMLElement | null = null

export function useDialog(options: DialogProps) {
  if (instance) {
    closeDialog()
  }

  container = document.createElement('div')
  document.body.appendChild(container)

  instance = createApp({
    render() {
      return h(LssDialog, {
        ...options,
        modelValue: true,
        onClose: () => {
          closeDialog()
        }
      })
    }
  })

  instance.mount(container)
}

function closeDialog() {
  if (instance && container) {
    instance.unmount()
    document.body.removeChild(container)
    instance = null
    container = null
  }
}
