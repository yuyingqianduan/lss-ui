import Viewer from './src/LssViewer.vue'
import { withInstall } from '../../withInstall'
const LssViewer = withInstall(Viewer)
export default LssViewer
