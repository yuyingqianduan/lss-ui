# LssEditor 富文本组件

富文本组件一般常用的功能主要是编辑文本、自定义上传图片或者视频，内容回显等常规处理的比较多，高度定制的得自行api扩展封装实现

## 基础用法

继承 `wangeditor` 所有属性 [wangeditor 参考文档](https://www.wangeditor.com/v5/getting-started.html)
:::demo
LssEditor/base
:::

## 自定义上传 视频 或者 图片 功能

:::demo
LssEditor/upload
:::

## 初始化/回显富文本编辑内容

:::demo
LssEditor/Detail
:::

## 常用配置参数（Attributes）继承 wangeditor Attributes

| 参数                  | 说明                                                               | 类型                       | 默认值 |
| :-------------------- | :----------------------------------------------------------------- | :------------------------- | :----- |
| model-value / v-model | 双向绑定值                                                         | any                        | -      |
| uploadImage           | 上传图片回调                                                       | `Promise<{ url: string }>` | -      |
| uploadVideo           | 上传视频回调                                                       | `Promise<{ url: string }>` | -      |
| toolbarConfig         | 工具栏配置                                                         | IToolbarConfig             | false  |
| editorConfig          | 编辑器配置                                                         | IEditorConfig              | false  |
| 更多...               | [参考官方文档](https://www.wangeditor.com/v5/getting-started.html) |                            |        |
