import { default as LssAddress } from './components/LssAddress';
import { default as LssSelect } from './components/LssSelect';
import { default as LssEditor } from './components/LssEditor';
import { default as LssPagination } from './components/LssPagination';
import { default as LssViewer } from './components/LssViewer';
import { default as LssDialog } from './components/LssDialog';
import { useDialog } from './components/LssDialog/src';
import { default as LssSeacrhForm } from './components/LssSeacrhForm';
import { default as LssTable } from './components/LssTable';
import { default as LssTablePlus } from './components/LssTablePlus';
import { installDirective } from './directives/index';

declare const lssUi: {
    install: any;
};
export default lssUi;
export { useDialog, installDirective };
export { LssAddress, LssSelect, LssEditor, LssPagination, LssViewer, LssDialog, LssSeacrhForm, LssTable, LssTablePlus };
