/*
 * @Descripttion: babel配置
 * @version:
 * @Author: lhl
 * @Date: 2024-08-14 20:27:40
 * @LastEditors: lhl
 * @LastEditTime: 2024-08-14 20:28:07
 */
module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  plugins: ['@vue/babel-plugin-jsx']
}
