import { Columns, SearchColumns } from './type';

type ContentAlign = 'left' | 'center' | 'right';
declare function __VLS_template(): Partial<Record<string, (_: any) => any>> & Partial<Record<string, (_: any) => any>> & {
    "header-buttons"?(_: {
        selectedList: any[];
        selectedListIds: number[];
        isSelected: (id: number) => boolean;
    }): any;
    "header-statistics"?(_: {
        total: number;
    }): any;
};
declare const __VLS_component: import('vue').DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToOption<{
    columns: Columns[];
    searcFiledsList?: SearchColumns[];
    requestFn?: Function;
    params?: object;
    hidePage?: boolean;
    hideBtn?: boolean;
    autoScroll?: boolean;
    emptyText?: string;
    contentAlign?: ContentAlign;
}>, {
    columns: () => never[];
    searcFiledsList: () => never[];
    requestFn: () => {};
    params: () => void;
    hidePage: boolean;
    hideBtn: boolean;
    autoScroll: boolean;
    emptyText: string;
    contentAlign: string;
}>, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    reset: (...args: any[]) => void;
    submit: (...args: any[]) => void;
}, string, import('vue').PublicProps, Readonly<globalThis.ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToOption<{
    columns: Columns[];
    searcFiledsList?: SearchColumns[];
    requestFn?: Function;
    params?: object;
    hidePage?: boolean;
    hideBtn?: boolean;
    autoScroll?: boolean;
    emptyText?: string;
    contentAlign?: ContentAlign;
}>, {
    columns: () => never[];
    searcFiledsList: () => never[];
    requestFn: () => {};
    params: () => void;
    hidePage: boolean;
    hideBtn: boolean;
    autoScroll: boolean;
    emptyText: string;
    contentAlign: string;
}>>> & {
    onReset?: ((...args: any[]) => any) | undefined;
    onSubmit?: ((...args: any[]) => any) | undefined;
}, {
    emptyText: string;
    contentAlign: ContentAlign;
    searcFiledsList: SearchColumns[];
    hideBtn: boolean;
    columns: Columns[];
    params: object;
    hidePage: boolean;
    autoScroll: boolean;
    requestFn: Function;
}, {}>;
declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, ReturnType<typeof __VLS_template>>;
export default _default;
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_WithTemplateSlots<T, S> = T & {
    new (): {
        $slots: S;
    };
};
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToOption<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
