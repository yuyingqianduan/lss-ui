declare const LssTablePlus: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        emptyText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        contentAlign: {
            type: globalThis.PropType<"left" | "right" | "center">;
            default: string;
        };
        searcFiledsList: {
            type: globalThis.PropType<import('./src/type').SearchColumns[]>;
            default: () => never[];
        };
        hideBtn: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        columns: {
            type: globalThis.PropType<import('./src/type').Columns<any>[]>;
            required: true;
            default: () => never[];
        };
        params: {
            type: globalThis.PropType<object>;
            default: () => void;
        };
        hidePage: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        autoScroll: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        requestFn: {
            type: globalThis.PropType<Function>;
            default: () => {};
        };
    }>> & {
        onReset?: ((...args: any[]) => any) | undefined;
        onSubmit?: ((...args: any[]) => any) | undefined;
    }, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
        reset: (...args: any[]) => void;
        submit: (...args: any[]) => void;
    }, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        emptyText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        contentAlign: {
            type: globalThis.PropType<"left" | "right" | "center">;
            default: string;
        };
        searcFiledsList: {
            type: globalThis.PropType<import('./src/type').SearchColumns[]>;
            default: () => never[];
        };
        hideBtn: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        columns: {
            type: globalThis.PropType<import('./src/type').Columns<any>[]>;
            required: true;
            default: () => never[];
        };
        params: {
            type: globalThis.PropType<object>;
            default: () => void;
        };
        hidePage: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        autoScroll: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        requestFn: {
            type: globalThis.PropType<Function>;
            default: () => {};
        };
    }>> & {
        onReset?: ((...args: any[]) => any) | undefined;
        onSubmit?: ((...args: any[]) => any) | undefined;
    }, {
        emptyText: string;
        contentAlign: "left" | "right" | "center";
        searcFiledsList: import('./src/type').SearchColumns[];
        hideBtn: boolean;
        columns: import('./src/type').Columns[];
        params: object;
        hidePage: boolean;
        autoScroll: boolean;
        requestFn: Function;
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        emptyText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        contentAlign: {
            type: globalThis.PropType<"left" | "right" | "center">;
            default: string;
        };
        searcFiledsList: {
            type: globalThis.PropType<import('./src/type').SearchColumns[]>;
            default: () => never[];
        };
        hideBtn: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        columns: {
            type: globalThis.PropType<import('./src/type').Columns<any>[]>;
            required: true;
            default: () => never[];
        };
        params: {
            type: globalThis.PropType<object>;
            default: () => void;
        };
        hidePage: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        autoScroll: {
            type: globalThis.PropType<boolean>;
            default: boolean;
        };
        requestFn: {
            type: globalThis.PropType<Function>;
            default: () => {};
        };
    }>> & {
        onReset?: ((...args: any[]) => any) | undefined;
        onSubmit?: ((...args: any[]) => any) | undefined;
    }, {}, {}, {}, {}, {
        emptyText: string;
        contentAlign: "left" | "right" | "center";
        searcFiledsList: import('./src/type').SearchColumns[];
        hideBtn: boolean;
        columns: import('./src/type').Columns[];
        params: object;
        hidePage: boolean;
        autoScroll: boolean;
        requestFn: Function;
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    emptyText: {
        type: globalThis.PropType<string>;
        default: string;
    };
    contentAlign: {
        type: globalThis.PropType<"left" | "right" | "center">;
        default: string;
    };
    searcFiledsList: {
        type: globalThis.PropType<import('./src/type').SearchColumns[]>;
        default: () => never[];
    };
    hideBtn: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    columns: {
        type: globalThis.PropType<import('./src/type').Columns<any>[]>;
        required: true;
        default: () => never[];
    };
    params: {
        type: globalThis.PropType<object>;
        default: () => void;
    };
    hidePage: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    autoScroll: {
        type: globalThis.PropType<boolean>;
        default: boolean;
    };
    requestFn: {
        type: globalThis.PropType<Function>;
        default: () => {};
    };
}>> & {
    onReset?: ((...args: any[]) => any) | undefined;
    onSubmit?: ((...args: any[]) => any) | undefined;
}, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    reset: (...args: any[]) => void;
    submit: (...args: any[]) => void;
}, string, {
    emptyText: string;
    contentAlign: "left" | "right" | "center";
    searcFiledsList: import('./src/type').SearchColumns[];
    hideBtn: boolean;
    columns: import('./src/type').Columns[];
    params: object;
    hidePage: boolean;
    autoScroll: boolean;
    requestFn: Function;
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & ((new () => {
    $slots: Partial<Record<string, (_: any) => any>> & Partial<Record<string, (_: any) => any>> & {
        "header-buttons"?(_: {
            selectedList: any[];
            selectedListIds: number[];
            isSelected: (id: number) => boolean;
        }): any;
        "header-statistics"?(_: {
            total: number;
        }): any;
    };
}) & import('vue').Plugin)))) & Record<string, any>;
export default LssTablePlus;
