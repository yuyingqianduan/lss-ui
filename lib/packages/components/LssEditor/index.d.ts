declare const LssEditor: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        modelValue: {
            type: globalThis.PropType<string>;
            required: true;
            default: string;
        };
        mode: {
            type: globalThis.PropType<string>;
            default: string;
        };
        uploadImage: {
            type: globalThis.PropType<(file: File) => Promise<{
                url: string;
            }>>;
            default: undefined;
        };
        uploadVideo: {
            type: globalThis.PropType<(file: File) => Promise<{
                url: string;
            }>>;
            default: undefined;
        };
        toolbarConfig: {
            type: globalThis.PropType<any>;
            default: {};
        };
        editorConfig: {
            type: globalThis.PropType<any>;
            default: {};
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    }, {
        editorRef: import('vue').ShallowRef<any>;
    }, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
        "update:modelValue": (...args: any[]) => void;
    }, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        modelValue: {
            type: globalThis.PropType<string>;
            required: true;
            default: string;
        };
        mode: {
            type: globalThis.PropType<string>;
            default: string;
        };
        uploadImage: {
            type: globalThis.PropType<(file: File) => Promise<{
                url: string;
            }>>;
            default: undefined;
        };
        uploadVideo: {
            type: globalThis.PropType<(file: File) => Promise<{
                url: string;
            }>>;
            default: undefined;
        };
        toolbarConfig: {
            type: globalThis.PropType<any>;
            default: {};
        };
        editorConfig: {
            type: globalThis.PropType<any>;
            default: {};
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    }, {
        modelValue: string;
        mode: string;
        uploadImage: (file: File) => Promise<{
            url: string;
        }>;
        uploadVideo: (file: File) => Promise<{
            url: string;
        }>;
        toolbarConfig: any;
        editorConfig: any;
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        modelValue: {
            type: globalThis.PropType<string>;
            required: true;
            default: string;
        };
        mode: {
            type: globalThis.PropType<string>;
            default: string;
        };
        uploadImage: {
            type: globalThis.PropType<(file: File) => Promise<{
                url: string;
            }>>;
            default: undefined;
        };
        uploadVideo: {
            type: globalThis.PropType<(file: File) => Promise<{
                url: string;
            }>>;
            default: undefined;
        };
        toolbarConfig: {
            type: globalThis.PropType<any>;
            default: {};
        };
        editorConfig: {
            type: globalThis.PropType<any>;
            default: {};
        };
    }>> & {
        "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
    }, {
        editorRef: import('vue').ShallowRef<any>;
    }, {}, {}, {}, {
        modelValue: string;
        mode: string;
        uploadImage: (file: File) => Promise<{
            url: string;
        }>;
        uploadVideo: (file: File) => Promise<{
            url: string;
        }>;
        toolbarConfig: any;
        editorConfig: any;
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    modelValue: {
        type: globalThis.PropType<string>;
        required: true;
        default: string;
    };
    mode: {
        type: globalThis.PropType<string>;
        default: string;
    };
    uploadImage: {
        type: globalThis.PropType<(file: File) => Promise<{
            url: string;
        }>>;
        default: undefined;
    };
    uploadVideo: {
        type: globalThis.PropType<(file: File) => Promise<{
            url: string;
        }>>;
        default: undefined;
    };
    toolbarConfig: {
        type: globalThis.PropType<any>;
        default: {};
    };
    editorConfig: {
        type: globalThis.PropType<any>;
        default: {};
    };
}>> & {
    "onUpdate:modelValue"?: ((...args: any[]) => any) | undefined;
}, {
    editorRef: import('vue').ShallowRef<any>;
}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    "update:modelValue": (...args: any[]) => void;
}, string, {
    modelValue: string;
    mode: string;
    uploadImage: (file: File) => Promise<{
        url: string;
    }>;
    uploadVideo: (file: File) => Promise<{
        url: string;
    }>;
    toolbarConfig: any;
    editorConfig: any;
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & import('vue').Plugin))) & Record<string, any>;
export default LssEditor;
