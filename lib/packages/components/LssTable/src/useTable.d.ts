interface TableRow {
    [key: string]: any;
}
interface UseTableProps {
    data?: TableRow[];
}
export declare function useTable(props: UseTableProps): {
    loading: globalThis.Ref<boolean>;
    data: globalThis.Ref<TableRow[]>;
    getTableData: () => Promise<void>;
};
export {};
