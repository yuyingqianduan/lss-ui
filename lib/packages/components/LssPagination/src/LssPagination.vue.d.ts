import { IProps } from './type';

declare const _default: import('vue').DefineComponent<__VLS_WithDefaults<__VLS_TypePropsToOption<IProps>, {
    pageParams: () => {
        page: number;
        size: number;
        total: number;
    };
    background: boolean;
    layout: string;
    pageSizes: () => number[];
}>, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {
    pagination: (...args: any[]) => void;
}, string, import('vue').PublicProps, Readonly<globalThis.ExtractPropTypes<__VLS_WithDefaults<__VLS_TypePropsToOption<IProps>, {
    pageParams: () => {
        page: number;
        size: number;
        total: number;
    };
    background: boolean;
    layout: string;
    pageSizes: () => number[];
}>>> & {
    onPagination?: ((...args: any[]) => any) | undefined;
}, {
    pageParams: import('./type').PageParams;
    layout: string;
    background: boolean;
    pageSizes: number[];
}, {}>;
export default _default;
type __VLS_WithDefaults<P, D> = {
    [K in keyof Pick<P, keyof P>]: K extends keyof D ? __VLS_Prettify<P[K] & {
        default: D[K];
    }> : P[K];
};
type __VLS_Prettify<T> = {
    [K in keyof T]: T[K];
} & {};
type __VLS_NonUndefinedable<T> = T extends undefined ? never : T;
type __VLS_TypePropsToOption<T> = {
    [K in keyof T]-?: {} extends Pick<T, K> ? {
        type: import('vue').PropType<__VLS_NonUndefinedable<T[K]>>;
    } : {
        type: import('vue').PropType<T[K]>;
        required: true;
    };
};
