export enum FormOptionsType {
  INPUT = 'input', // 输入框
  SELECT = 'select', // 下拉框
  CASCADER = 'cascader', // 级联选择器
  DATE_PICKER = 'daterange' // 默认日期范围
}

// 定义 SearchFormType 类型
export interface SearchFormType {
  [key: string]: any // 搜索表单的键值对
}
