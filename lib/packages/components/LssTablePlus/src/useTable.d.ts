import { Ref } from 'vue';
import { Columns, SearchColumns } from './type';

interface UseTableOptions {
    columns: Columns[];
    searcFiledsList?: SearchColumns[];
    requestFn?: Function;
    params?: object;
    hidePage?: boolean;
    hideBtn?: boolean;
    autoScroll?: boolean;
}
export declare function useTable(options: UseTableOptions, emits: any): {
    loading: Ref<boolean>;
    data: Ref<any[]>;
    searchForm: Ref<{}>;
    tableParams: Ref<{
        page: number;
        pageSize: number;
        total: number;
    }>;
    handleSearch: () => void;
    handleReset: () => void;
    handlePageChange: (pageParams: any) => void;
    getTableData: () => Promise<void>;
};
export {};
