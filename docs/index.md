---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: 'LSS-UI 库'
  text: 'VitePress静态站点'
  tagline: Personal UI Component Library （个人UI组件库，胆小勿入）
  image:
    src: /img/cxy_boy.png
    alt: lss-ui
  actions:
    - theme: brand
      text: 马上开始
      link: /home/getting-started
    - theme: alt
      text: UI组件案例及用法
      link: /components/intro

features:
  - icon: 📚
    title: 个人UI库
    details: 命名为 lss-ui，基于 Vue3 + Typescript + element-plus + Vite5 二次封装，主要是为了方便自己开发项目时使用，提高开发效率
  - icon: 🎨
    title: 二次封装的element-plus 以及一些常用组件 （pc端）
    details: 涵盖了一些基础的组件二次封装，如：输入框、表格、弹框、表单、上传组件、下拉框、富文本组件等等
  - icon: 🚀
    title: 一起立个flag，共建更好的UI组件库
    details: 每天撸一个，想到哪写到哪，欢迎大家一起来完善。主要是写的不足的地方，欢迎大家提出意见。平时项目经验基于实际应用而封装的组件方便大家使用
  - icon: 🎉
    title: 最新技术
    details: 享受 Vue3.4 + vite5 + Typescript 的开发体验，在 Markdown 中使用 Vue 组件，同时可以使用 Vue 来开发自定义主题
---

