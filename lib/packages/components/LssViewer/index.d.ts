declare const LssViewer: ({
    new (...args: any[]): import('vue').CreateComponentPublicInstance<Readonly<globalThis.ExtractPropTypes<{
        noDataText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        imgList: {
            type: globalThis.PropType<string[]>;
            required: true;
            default: () => never[];
        };
        btnText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        viewerOptions: {
            type: globalThis.PropType<Record<string, any>>;
            default: () => {};
        };
    }>>, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {}, import('vue').VNodeProps & import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & Readonly<globalThis.ExtractPropTypes<{
        noDataText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        imgList: {
            type: globalThis.PropType<string[]>;
            required: true;
            default: () => never[];
        };
        btnText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        viewerOptions: {
            type: globalThis.PropType<Record<string, any>>;
            default: () => {};
        };
    }>>, {
        noDataText: string;
        imgList: string[];
        btnText: string;
        viewerOptions: Record<string, any>;
    }, true, {}, {}, {
        P: {};
        B: {};
        D: {};
        C: {};
        M: {};
        Defaults: {};
    }, Readonly<globalThis.ExtractPropTypes<{
        noDataText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        imgList: {
            type: globalThis.PropType<string[]>;
            required: true;
            default: () => never[];
        };
        btnText: {
            type: globalThis.PropType<string>;
            default: string;
        };
        viewerOptions: {
            type: globalThis.PropType<Record<string, any>>;
            default: () => {};
        };
    }>>, {}, {}, {}, {}, {
        noDataText: string;
        imgList: string[];
        btnText: string;
        viewerOptions: Record<string, any>;
    }>;
    __isFragment?: never;
    __isTeleport?: never;
    __isSuspense?: never;
} & import('vue').ComponentOptionsBase<Readonly<globalThis.ExtractPropTypes<{
    noDataText: {
        type: globalThis.PropType<string>;
        default: string;
    };
    imgList: {
        type: globalThis.PropType<string[]>;
        required: true;
        default: () => never[];
    };
    btnText: {
        type: globalThis.PropType<string>;
        default: string;
    };
    viewerOptions: {
        type: globalThis.PropType<Record<string, any>>;
        default: () => {};
    };
}>>, {}, unknown, {}, {}, import('vue').ComponentOptionsMixin, import('vue').ComponentOptionsMixin, {}, string, {
    noDataText: string;
    imgList: string[];
    btnText: string;
    viewerOptions: Record<string, any>;
}, {}, string, {}> & (import('vue').VNodeProps & (import('vue').AllowedComponentProps & import('vue').ComponentCustomProps & import('vue').Plugin))) & Record<string, any>;
export default LssViewer;
