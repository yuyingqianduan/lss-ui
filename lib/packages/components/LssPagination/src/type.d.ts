export interface PageParams {
    page: number;
    size: number;
    total: number;
}
export interface IProps {
    pageParams: PageParams;
    layout?: string;
    background?: boolean;
    pageSizes?: number[];
}
