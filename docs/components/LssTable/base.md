# LssTable 普通表格组件

## 基础用法

继承 `el-table` 所有属性 配置 tableData 数据源即可支持个性化表头，jsx渲染（结合h函数）
:::demo
LssTable/base
:::

## 定制化表格

继承 `el-table` 所有属性 配置 tableData 数据源即可支持个性化表头，支持跨页勾选、回显（传入initialSelectedIds 唯一标识组成的list即可）、jsx渲染（结合h函数）、展开收起、自定义操作列、操作栏、上移下移、列设置等
:::demo
LssTable/custom
:::

## 定制化表格 - 多级表头

继承 `el-table` 所有属性 配置 tableData 数据源即可支持个性化表头，支持跨页勾选、回显（传入initialSelectedIds 唯一标识组成的list即可）、jsx渲染（结合h函数）、展开收起、自定义操作列、操作栏、上移下移、列设置等
:::demo
LssTable/MultiTable
:::

::: tip
多级表头和固定列存在问题官方组件也是如此不建议同时使用，多级表头不支持固定列
:::

## 常用配置参数（Attributes）继承 el-table（组件） Attributes

| 参数                     | 说明                                                                     | 类型                        | 默认值   |
| :----------------------- | :----------------------------------------------------------------------- | :-------------------------- | :------- |
| columns                  | 单级表头数据 （表头数据二选一配置即可）                                  | []                          | []       |
| columnGroups （新）      | 多级表头数据 （表头数据二选一配置即可）fixed属性有bug官网如此            | []                          | []       |
| emptyText （新）         | 表格无数据文案                                                           | string                      | 暂无数据 |
| noValueText （新）       | 列无数据文案                                                             | string                      | /        |
| contentAlign（新）       | 对齐方式，默认居中                                                       | 'left' / 'center' / 'right' | center   |
| uniqueKey（新）          | 唯一标识 （用于选中/回显数据）                                           | string                      | id       |
| showColumnSetting        | 是否启用列设置（对单级表头生效）                                         | boolean                     | false    |
| showMove（新）           | 是否启用上移下移功能                                                     | boolean                     | false    |
| initialSelectedIds（新） | 初始化选中的数据                                                         | string[]/number[]           | []       |
| data                     | 渲染表格所需的数据                                                       | any[]                       | []       |
| 更多...                  | [参考官方文档](https://element-plus.org/zh-CN/component/pagination.html) |                             |          |

## 暴露给父组件的属性和方法

- tableRef 表格 ref
- setInitialSelectedIds 初始化选中的方法
- isSelected 当前选中的数据是否存在
- selectedList 当前选中的数据列表
- selectedListIds 当前选中的数据唯一标识列表
- handleSelectTableData 选中数据的方法
- handleSelect 选中数据的方法
- handleSelectAll 全选/反选方法
- handleClear 清空选中数据的方法
- visibleColumns 当前可见的列
- moveUp 上移方法
- moveDown 下移方法
