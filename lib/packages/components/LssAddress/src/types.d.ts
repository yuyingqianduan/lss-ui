export interface IProps {
    options: any[];
    checkStrictly?: boolean;
    labelKey?: string;
    valueKey?: string;
}
