import { AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig } from 'axios';

export type RequestInterceptor = (config: InternalAxiosRequestConfig) => InternalAxiosRequestConfig | Promise<InternalAxiosRequestConfig>;
export type RequestErrorInterceptor = (error: any) => any;
export type ResponseInterceptor = (response: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>;
export type ResponseErrorInterceptor = (error: any) => any;
export interface AxiosPluginConfig {
    baseURL?: string;
    timeout?: number;
    requestInterceptors?: RequestInterceptor[];
    requestErrorInterceptors?: RequestErrorInterceptor[];
    responseInterceptors?: ResponseInterceptor[];
    responseErrorInterceptors?: ResponseErrorInterceptor[];
    errorHandler?: (error: any) => void;
    successHandler?: (response: AxiosResponse) => void;
    commonHeaders?: Record<string, string>;
    cacheEnabled?: boolean;
    retryCount?: number;
    loadingEnabled?: boolean;
    loadingPlugin?: any;
    loadingOptions?: any;
    toastEnabled?: boolean;
    toastOptions?: any;
    maxConcurrentRequests?: number;
}
declare module 'axios' {
    interface InternalAxiosRequestConfig {
        commonHeaders?: Record<string, string>;
        loadingInstance?: any;
        toastEnabled?: boolean;
        loadingOptions?: any;
        loadingEnabled?: boolean;
        retryCount?: number;
        toastOptions?: any;
    }
}
export default class AxiosPlugin {
    private static instance;
    private service;
    private requestCache;
    private responseCache;
    private pendingRequests;
    private maxConcurrentRequests;
    private asyncQueue;
    private constructor();
    static getInstance(config: AxiosPluginConfig): AxiosPlugin;
    private setupInterceptors;
    private processQueue;
    get(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse>;
    post(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse>;
    put(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse>;
    delete(url: string, config?: AxiosRequestConfig): Promise<AxiosResponse>;
    patch(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse>;
}
